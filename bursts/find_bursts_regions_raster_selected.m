function [ data ] = find_bursts_regions_raster_selected( data, varargin )
%find_bursts_regions_raster_selected find the occurence of bursting events
% We will consider a burst to have happened when more than 30% of the cells
% fire at the same frame.
% data.activity_fraction: fraction of cells that fired at the same time
% data.burst_time       : time of bursting events
% data.interburst_time  : time between two consecutive bursts

global RUN

if ~RUN.from_image
    return;
end

out = get_bursts_for_regions(data, data.regions_raster_selected, varargin);

data.regions_raster_selected_activity_fraction  = out.activity_fraction;
data.regions_raster_selected_burst_threshold    = out.burst_threshold;
data.regions_raster_selected_burst_time         = out.burst_time;
data.regions_raster_selected_interburst_time    = out.interburst_time;

% stats
data.regions_raster_selected_mean_interburst                = out.mean_interburst;
data.regions_raster_selected_std_interburst                 = out.std_interburst;
data.regions_raster_selected_mean_burst_activity_fraction   = out.mean_burst_activity_fraction;
data.regions_raster_selected_std_burst_activity_fraction    = out.std_burst_activity_fraction;

% per region
data.regions_raster_selected_region_burst_time         = out.region_burst_time;
data.regions_raster_selected_region_interburst_time    = out.region_interburst_time;
data.regions_raster_selected_region_mean_interburst                = out.region_mean_interburst;
data.regions_raster_selected_region_std_interburst                 = out.region_std_interburst;  
data.regions_raster_selected_region_mean_burst_activity_fraction   = out.region_mean_burst_activity_fraction;
data.regions_raster_selected_region_std_burst_activity_fraction    = out.region_std_burst_activity_fraction;
end