function [ out ] = get_bursts_for_regions( data, regions, args )
%get_bursts_for_regions Finds burst stats for given regions

global CONFIG
global RUN

% work region by region

n_regions = size(regions,2); % how many do we have?
activity_fraction = cell(n_regions, 1);
burst_events = cell(n_regions, 1);
cells_recruited = cell(n_regions, 1);

for i=1:n_regions
    % current region
    r = regions(:,i); 
    a = r(1);b = r(2);
    
    % get firing events for the region
    frames = firing_map_for_interval(data, a, b);

    % figure out active cells
    active_cells = sum(frames, 2) > CONFIG.min_firing_for_active;
    
    % get the activity fraction
    firing_count = sum(frames(active_cells, :), 1);
    af = firing_count / sum(active_cells);
    
    % find burst events
    be = find(af > CONFIG.burst_threshold & firing_count > 3);
    
    % find the cells that fired on each burst
    cr = cell(length(be), 1);
    for evt = 1:length(be)
        cr{evt} = find(frames(:,be(evt)));
    end % for firing events
    
    % export
    activity_fraction{i} = af;
    burst_events{i} = (be + a - 1);% ./ RUN.fps; % convert to seconds and store
    cells_recruited{i} = cr;
end % for regions

%% data output
out = struct();
out.activity_fraction = activity_fraction;
out.burst_time        = burst_events;
out.burst_cells       = cells_recruited;

% out = struct();
% out.activity_fraction  = af;
% out.burst_threshold    = thr;
% out.burst_time         = b_times;
% out.interburst_time    = ib_times;
% 
%% stats
% out.mean_interburst                = mean_ib;
% out.std_interburst                 = std_ib;
% out.mean_burst_activity_fraction   = mean_ac;
% out.std_burst_activity_fraction    = std_ac;
% 
%% per region
% out.region_burst_time         = b_r;
% out.region_interburst_time    = ib_r;
% out.region_mean_interburst                = mean_ib_r;
% out.region_std_interburst                 = std_ib_r;
% out.region_mean_burst_activity_fraction   = mean_ac_r;
% out.region_std_burst_activity_fraction    = std_ac_r;

end

function c = append_to(c, a)
    c{length(c)+1} = a;
end
  
