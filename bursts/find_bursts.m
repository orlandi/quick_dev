function [ data ] = find_bursts( data, varargin )
%FIND_BURSTS find the occurence of bursting events on data
% We will consider a burst to have happened when more than 30% of the cells
% fire at the same frame.
% data.activity_fraction: fraction of cells that fired at the same time
% data.burst_time       : time of bursting events
% data.interburst_time  : time between two consecutive bursts

out = get_bursts_for_regions(data, data.regions, varargin);

data.activity_fraction  = out.activity_fraction;
% data.burst_threshold    = out.burst_threshold;
data.burst_time         = out.burst_time;
% data.interburst_time    = out.interburst_time;

% stats
data.mean_interburst                = out.mean_interburst;
data.std_interburst                 = out.std_interburst;
data.mean_burst_activity_fraction   = out.mean_burst_activity_fraction;
data.std_burst_activity_fraction    = out.std_burst_activity_fraction;

% per region
data.region_burst_time         = out.region_burst_time;
data.region_interburst_time    = out.region_interburst_time;
data.region_mean_interburst                = out.region_mean_interburst;
data.region_std_interburst                 = out.region_std_interburst;  
data.region_mean_burst_activity_fraction   = out.region_mean_burst_activity_fraction;
data.region_std_burst_activity_fraction    = out.region_std_burst_activity_fraction;
end