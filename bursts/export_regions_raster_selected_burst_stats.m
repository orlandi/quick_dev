function export_regions_raster_selected_burst_stats( fout, data )
%EXPORT_BURST_STATS writes down a section with data about the bursts
% all members related to bursts are exported

global RUN

if ~RUN.from_image
    return;
end

fprintf(fout, '##\n##burst stats selected regions\n##\n');

fprintf(fout, 'average_interburst_time:%f\n', ...
    data.regions_raster_selected_mean_interburst);
fprintf(fout, 'standard_deviation_interburst_time:%f\n', ...
    data.regions_raster_selected_std_interburst);
fprintf(fout, 'average_burst_frequency:%f\n', ...
    1 ./ data.regions_raster_selected_mean_interburst);
fprintf(fout, 'average_activity_fraction_on_burst:%f\n',...
    data.regions_raster_selected_mean_burst_activity_fraction);
fprintf(fout, 'standard_deviation_activity_fraction_on_burst:%f\n',...
    data.regions_raster_selected_std_burst_activity_fraction);

% per region
for i=1:length(data.regions_raster_selected_region_mean_interburst)
    fprintf(fout, '#region %d\n', i);
    fprintf(fout, 'average_interburst_time:%f\n', ...
        data.regions_raster_selected_region_mean_interburst(i));
    fprintf(fout, 'standard_deviation_interburst_time:%f\n', ...
        data.regions_raster_selected_region_std_interburst(i));
    fprintf(fout, 'average_burst_frequency:%f\n', ...
        1 ./ data.regions_raster_selected_region_mean_interburst(i));
    fprintf(fout, 'average_activity_fraction_on_burst:%f\n',...
        data.regions_raster_selected_region_mean_burst_activity_fraction(i));
    fprintf(fout, 'standard_deviation_activity_fraction_on_burst:%f\n',...
        data.regions_raster_selected_region_std_burst_activity_fraction(i));
end
end
