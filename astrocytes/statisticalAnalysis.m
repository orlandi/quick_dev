% select loaded matlab file
defaultPath = 'C:\Users\colicos\Desktop\Xining\LindenData';
[fileName, filePath] = uigetfile(defaultPath, '*.mat');

fullFileName = fullfile(filePath, fileName);
storedData = load(fullFileName);

glialEvents = storedData.glialEvents;
firingFrames = storedData.firingFrames;

% define field of view size (pixels by pixels):
fov_x = 32;
fov_y = 32;
frameSize = 512;
arraySize = frameSize/fov_x;
avg_frame = 50;
overlapFrames = 25;

% counting structure
corrCounts = struct('glia', zeros(arraySize, arraySize), ...
    'neuron', zeros(arraySize, arraySize));

% go through each 50 frames of firingFrames and start counting neuron
% firings
for i=1:length(glialEvents)
    lb = 1+(i-1)*overlapFrames; ub = lb + avg_frame;
    corrCounts(i).neuron = zeros(arraySize, arraySize); % initialize
	corrCounts(i).glia = zeros(arraySize, arraySize);
    
    % counting glia
    if ~isempty(glialEvents{i})
       for k=1:length(glialEvents{i})
          tempX = floor(glialEvents{i}(k).center(1)/fov_x)+1;
          tempY = floor(glialEvents{i}(k).center(2)/fov_y)+1;
          
          corrCounts(i).glia(tempX, tempY) = corrCounts(i).glia(tempX, tempY) + 1;
       end
    end
    
    % counting neurons
    for j=lb:ub
        if ~isempty(firingFrames(j).x)      % not empty, record events
           for l=1:length(firingFrames(j).x)
               tempX = floor(firingFrames(j).x(l)/fov_x)+1;
               tempY = floor(firingFrames(j).y(l)/fov_y)+1;
               
               corrCounts(i).neuron(tempX, tempY) = corrCounts(i).neuron(tempX, tempY) +1;
           end
        end
    end
end

% plotting collected data
X = zeros(1, arraySize^2);
Y = zeros(1, arraySize^2);
Z = zeros(1, arraySize^2);
neuronSize = zeros(1, arraySize^2); % these are the counts
gliaSize = zeros(1, arraySize^2);

figure
for t=1:length(corrCounts)
   it = 1;
    for iX=1:arraySize
        for iY=1:arraySize
            X(it)=iX;
            Y(it)=iY;
            Z(it)=t;
            neuronSize(it) = corrCounts(t).neuron(iX, iY)*10+0.1;
            gliaSize(it) = corrCounts(t).glia(iX, iY)*10+0.1;
            
            it = it+1;
        end
    end
    
    scatter3(X, Y, Z, neuronSize, 'r', 'filled'); hold on;
    scatter3(X, Y, Z, gliaSize, 'b', 'filled');
end
xlabel('X position');
ylabel('Y position');
zlabel('Average time');