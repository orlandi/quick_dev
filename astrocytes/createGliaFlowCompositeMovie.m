function createGliaFlowCompositeMovie(data)
    global CONFIG RUN;
    fID_u = fopen(data.gliaUDataFile, 'r');
    fID_v = fopen(data.gliaVDataFile, 'r');
    fID_avg = fopen(data.gliaAverageDataFile, 'r');
    Nframes = data.newFrames;
    avgFrame = data.frame;
    maxR = data.globalMaxMag;
    minIntensity = RUN.avgFrameMinIntensity;
    maxIntensity = RUN.avgFrameMaxIntensity;
    
    f_path = [RUN.video_path 'astrocyte' filesep];
    if ~exist(f_path, 'file'), mkdir(f_path);end;
    
    newMovie = VideoWriter(RUN.gliaCompositeMovieFile, 'Uncompressed AVI');
    open(newMovie);
    ct = tic;
    textprogressbar('Recording RGB flow movie: ');
    for i = 1:Nframes
        u = fread(fID_u, size(avgFrame), 'double');
        v = fread(fID_v, size(avgFrame), 'double');
        avgFrame = fread(fID_avg, size(avgFrame), 'double');
        if(CONFIG.useLUTcorrection)
            rescaledFrame = (avgFrame-minIntensity)/(maxIntensity-minIntensity);
            rescaledFrame(rescaledFrame > 1) = 1;
            rescaledFrame(rescaledFrame < 0) = 0;
        else
            rescaledFrame = rescale_image(avgFrame);
        end
        currFrame = zeros([size(rescaledFrame) 3]);
        currFrame(:, :, 1) = rescaledFrame;
        r = sqrt(u.^2+v.^2)/maxR;
        theta = (atan2(v,u)+pi)/2/pi;
        % If the modulus is too small (< 1%), assign theta = 0
        theta(r < 0.01) = 0;
        currFrame(:, :, 2) = r;
        currFrame(:, :, 3) = theta;
        writeVideo(newMovie, currFrame);
        textprogressbar(i/Nframes*100);
    end
    textprogressbar(sprintf('Done! (%.2f s)', toc(ct)));

close(newMovie);
fclose(fID_u);
fclose(fID_v);
fclose(fID_avg);

end