function [ newU, newV ] = firingFilter( u, v, globalMaxMag, astrocyte, rSize )
%% firing filter
% if u_i and v_i are dark blue but u and v are red, set these values to 0
% because most likely it is an artifact of the neurons firing brightly and
% causing a light up in it's surrounding. The neuron detection algorithm
% doesn't fully remove such activities since the mask it creates for the
% neurons are too small to remove the surrounding light. Hence we will
% filter such effects here.
%-------------------------------------------------------------------------
blueColorThreshold = ones(size(u(1).vector))*globalMaxMag*0.05;
greenColorThreshold = ones(size(u(1).vector))*globalMaxMag*0.2;

% % Enhance the quiver plot visually by showing one vector per region
% for t=1:size(u,2)
%     for i=1:size(u(t).vector,1)
%         for j=1:size(u(t).vector,2)
%             if floor(i/rSize)~=i/rSize || floor(j/rSize)~=j/rSize
%                 u(t).vector(i,j)=0;
%                 v(t).vector(i,j)=0;
%             end
%         end
%     end
% end

newU = struct('vector', zeros(size(u(1).vector)));
newV = struct('vector', zeros(size(u(1).vector)));
newU(1).vector = u(1).vector;
newV(1).vector = v(1).vector;

tic;
for i=2:size(u,2)-2
    
%     if i==17
%         disp('stop');
%     end
    
    currentMag = sqrt(u(i).vector.*u(i).vector + v(i).vector.*v(i).vector);        % magnitudes for (i)
    previousMag = sqrt(u(i-1).vector.*u(i-1).vector + v(i-1).vector.*v(i-1).vector); % mag for (i-1)
    nextMag = sqrt(u(i+1).vector.*u(i+1).vector + v(i+1).vector.*v(i+1).vector);    % mag for (i+1)
    
    %if previous mag is blue, and currently it's green or brighter
%     mask = and(currentMag >= greenColorThreshold, previousMag <= blueColorThreshold);
%     
%     if any(mask(:)>0)           % forward sharp discrepency.
%         additionalFrame = nextMag > blueColorThreshold;
%         if any(additionalFrame(:) > 0)    % use i+2 frame for next
%             nextMag2 = sqrt(u(i+2).vector.*u(i+2).vector + v(i+2).vector.*v(i+2).vector);    % mag for (i+2)
%             mask2 = and(nextMag >= greenColorThreshold, nextMag2 <= blueColorThreshold);
%             mask = or(mask, mask2);   % suddenly goes back to blue
%         else                    % just use 1 next frame
%             mask2 = and(currentMag >= greenColorThreshold, nextMag2 <= blueColorThreshold);
%             mask = or(mask, mask2);    %then the next mag is again blue
%         end      
%         mask2 = bwmorph(mask, 'remove');
%         se = strel('octagon', 9);
%         mask2 = imdilate(mask2, se);
 
%         maskArea = bwmorph(mask, 'area');
        
%         mask2 = bwmorph(mask, 'thicken', 3);

%         se = strel('octagon', 9);
%         mask2 = imdilate(mask2, se);
%         newU(i).vector = u(i).vector;
%         newV(i).vector = v(i).vector;
%         newU(i).vector(mask2) = 0;
%         newV(i).vector(mask2) = 0;
%     else                % check for backward sharp discrepency
        mask2 = and(currentMag >= greenColorThreshold, nextMag <= blueColorThreshold);
        if any(mask2(:)>0)
%             mask2 = bwmorph(mask, 'remove');
            se = strel('octagon', 9);
            mask2 = imdilate(mask2, se);
            
            newU(i).vector = u(i).vector;
            newV(i).vector = v(i).vector;
            newU(i).vector(mask2) = 0;
            newV(i).vector(mask2) = 0;
        else
            newU(i).vector = u(i).vector;
            newV(i).vector = v(i).vector;
        end
%     end
    
end
disp('end of firing filter'); toc;

for i=size(u,2)-2:size(u,2)
    newU(i).vector = u(i).vector;
    newV(i).vector = v(i).vector;
end


