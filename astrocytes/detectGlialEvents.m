function [ glialEvents ] = detectGlialEvents(uFile, vFile, averageFile, localMaxMag, globalMaxMag, n, frame, Nframes)
%DETECTGLIALEVENTS: using the optical flow vectors and the intensity
%thresholds we've set in startup, we try to detect where a glial event
%might have initiated from. 
%   uFile, vFile, averageFile: binary files containing u and v fields and
%   the coarse grained intensity
%   localMaxMag: a vector containing the maximum magnitude of the flow
%   vectors for each frame. 
%   globalMaxMag: a vector containing the maximum magnitude throughout all
%   the frames. (aka this is the biggest "change in intensity" of the
%   entire movie.)
%   n: the indexing number for output files. 
%   frame:
%   Nframes:

global CONFIG RUN

% mag and astrocites movie 


%incidentEventFile = zeros([size(frame) Nframes]);



%% Find incident glial events using intensity threshold
% Load the files
gliaUDataFile = fopen(uFile, 'r');
gliaVDataFile = fopen(vFile, 'r');
incidentEventFile = fopen([RUN.dump_path RUN.name '_gliaIncidentEvents.dat'], 'w');
%gliaAverageDataFile = fopen(averageFile, 'r');
ct = tic;
textprogressbar('Detecting initial glial events using intensity threshold: ');
for t=1:Nframes
    u = fread(gliaUDataFile, size(frame), 'double');
    v = fread(gliaVDataFile, size(frame), 'double');
    tempMag = sqrt(u.^2+v.^2);
    incidentEvent = zeros(size(tempMag));
    if localMaxMag(t) >=  CONFIG.glial_globalI_threshold*globalMaxMag
        %possible incident event.
        tempMag(tempMag <= localMaxMag(t)*CONFIG.glial_localI_threshold)=0;
        tempMag(tempMag > localMaxMag(t)*CONFIG.glial_localI_threshold)=1;
        incidentEvent = tempMag;
    end
    fwrite(incidentEventFile, incidentEvent, 'double');
    textprogressbar(t/Nframes*100);
end
textprogressbar(sprintf('Done! (%.2f s)', toc(ct)));
fclose(gliaUDataFile);
fclose(gliaVDataFile);
fclose(incidentEventFile);

%% Filter glial events by area threshold
glialEvents = filterByArea([RUN.dump_path RUN.name '_gliaIncidentEvents.dat'], n, frame, Nframes);

%% Visualization of glial events

%--- structure for setting tiff tags
% tagstruct.Photometric = Tiff.Photometric.RGB;
% tagstruct.BitsPerSample = 8;
% tagstruct.SamplesPerPixel = 3;
% tagstruct.PlanarConfiguration = Tiff.PlanarConfiguration.Chunky;

str = strcat(RUN.video_path, RUN.name, '_glialEvents_', n);
%t = Tiff(str, 'w');
newMovie = VideoWriter(str, 'Uncompressed AVI');
open(newMovie);

figSize = size(frame);
gliaAverageDataFile = fopen(averageFile, 'r');
ct = tic;
textprogressbar('Visualizing the final incident glial events: ');
for l=1:Nframes
    astrocitesMovie = fread(gliaAverageDataFile, size(frame), 'double');
    hFig = figure();
    set(hFig, 'visible', 'off');
    axis([0 figSize(2) 0 figSize(1)]);
    if(CONFIG.useLUTcorrection)
        minIntensity = RUN.avgFrameMinIntensity;
        maxIntensity = RUN.avgFrameMaxIntensity;
    else
        minIntensity = 0;
        maxIntensity = 2^RUN.bitDepth-1;
    end
    imshow(astrocitesMovie, [minIntensity maxIntensity]); hold on;
    %     contour(incidentEvent(i).vector);
    for j=1:length(glialEvents{l})
        scatter(glialEvents{l}(j).center(2), glialEvents{l}(j).center(1),40, [0.0, 1.0, 1.0]);
    end
    set(hFig,'units','normalized','outerposition',[0 0 1 1]);    % "fullscreen"
    set(gca,'YDir','reverse');
    hold off;
    
    currFrame = getframe(hFig);
    
    close(hFig);
    %-- tiff storage tags
    %tagstruct.ImageLength = size(currFrame.cdata,1);
    %tagstruct.ImageWidth = size(currFrame.cdata,2);
    %t.setTag(tagstruct);
    %t.write(currFrame.cdata);
    %writeDirectory(t);
    writeVideo(newMovie, currFrame.cdata);
    textprogressbar(l/Nframes*100);
end
textprogressbar(sprintf('Done! (%.2f s)', toc(ct)));
%t.close();
close(newMovie);
fclose(gliaAverageDataFile);
delete([RUN.dump_path RUN.name '_gliaIncidentEvents.dat']);
end


