function saveResults( C, totalN, totalG, alpha, stats, n)
%SAVERESULTS Summary of this function goes here
%   Detailed explanation goes here

global RUN CONFIG

f_path = [RUN.dump_path 'text' filesep];
if ~exist(f_path, 'file'), mkdir(f_path);end;

n_data = [RUN.name '_alphaValue_' num2str(n)]; 
fileName = [f_path n_data '.txt'];
fID = fopen(fileName, 'w');

iMax = size(alpha, 1);
jMax = size(alpha, 2);

fprintf(fID, 'x\ty\ttotalG\ttotalN\tC\tC/totalN\tmean\tstDev\tMax\tNNZ\n');
fmt1 = '%8.4f\t%8.4f\t%8.4f\t%8.4f\t%8.4f\t%8.4f\t%8.4f\t%8.4f\t%8.4f\t%8.4f\n';
fmt2 = '%8.4f\t%8.4f\t%8.4f\t%8.4f\t%8.4f\t%8.4f\n';

for i=1:iMax
    for j=1:jMax
        if i==1 && j==1
            fprintf(fID, fmt1, ...
                i, j, totalG(i,j), totalN(i,j), C(i,j), alpha(i,j), ...
                stats.mean, stats.stdev, stats.max, stats.nnz);
        else
            fprintf(fID, fmt2, ...
                i, j, totalG(i,j), totalN(i,j), C(i,j), alpha(i,j));
        end
    end
end

fclose(fID);

% Also save a text file for the glial detection parameters used for this
% set of text data
n_data = ['CONFIG_parameters_' num2str(n)]; 
fileName = [f_path n_data '.txt'];
fID = fopen(fileName, 'w');
fmt = '%s:\t%4.2f\n';

names = fieldnames(CONFIG);     

% start @13-th parameter. 
for l=12:length(names)-3                 % ignore the worklists
   name = char(names(l));
   nameValue = getfield(CONFIG, name);
   
   if isa(nameValue, 'float')
        fprintf(fID, fmt, name, nameValue); 
   end
end

fclose(fID);

end

