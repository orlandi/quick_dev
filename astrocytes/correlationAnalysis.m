function [ glialEvents ] = correlationAnalysis(data)
%CORRELATIONANALYSIS: count the number of times a glia firing happens at
%the same time as a neuron firing AND the glia initiation is within a
%certain distance from the neuron firing. 
%   data: data containing when optical flow speeds and neuron firings. 
%   glialEvents: --- the output variable is unnecessary atm. May be removed
%   or potentially be changed later. 

global CONFIG

disp('Correlation analysis...');

n=num2str(CONFIG.N);
firingFrames = data.firingFrames; avg_frame = data.avgFrameRate; 

glialEvents = detectGlialEvents(data.gliaUDataFile, data.gliaVDataFile, data.gliaAverageDataFile, data.localMaxMag, data.globalMaxMag, n, data.frame, data.newFrames);

%% Correlation 
% define field of view size (pixels by pixels):
fov_x = CONFIG.fov_x;
fov_y = CONFIG.fov_y;
frameSize = size(data.mean_frame,1);
arraySize = frameSize/fov_x;
overlapFrames = floor(avg_frame/2);
totalNeuronFiring=0;

% counting structure
%corrCounts = struct('glia', zeros(arraySize, arraySize), ...
%    'neuron', zeros(arraySize, arraySize));
corrCountsNeuron = zeros(arraySize, arraySize, data.newFrames);
corrCountsGlia = zeros(arraySize, arraySize, data.newFrames);

tic
disp('Starting correlation analysis');
% go through each 50 frames of firingFrames and start counting neuron
% firings and glia events
%ct = tic;
for i=1:data.newFrames
    lb = 1+(i-1)*overlapFrames; ub = lb + avg_frame;
    %corrCounts(i).neuron = zeros(arraySize, arraySize); % initialize
	%corrCounts(i).glia = zeros(arraySize, arraySize);
    
    % counting glia events in spacial buckets
    if ~isempty(glialEvents{i})
       for k=1:length(glialEvents{i})
          tempX = floor(glialEvents{i}(k).center(1)/fov_x)+1;
          tempY = floor(glialEvents{i}(k).center(2)/fov_y)+1;
          
          corrCountsGlia(tempX, tempY, i) = corrCountsGlia(tempX, tempY, i) + 1;
       end
    end
    
    % counting neurons in spcial buckets
    for j=lb:ub
        if ~isempty(firingFrames(j).x)      % not empty, record events
           for l=1:length(firingFrames(j).x)
               tempX = floor(firingFrames(j).x(l)/fov_x)+1;
               tempY = floor(firingFrames(j).y(l)/fov_y)+1;
               
               % checks to ensure index within array size bounds.
               if tempX > arraySize
                   tempX=tempX-1;
               elseif tempY > arraySize
                   tempY=tempY-1;
               end
               
               corrCountsNeuron(tempX, tempY, i) = corrCountsNeuron(tempX, tempY, i) +1;
               totalNeuronFiring = totalNeuronFiring + 1;
           end
        end
    end
end

%%
saveEventFig(corrCountsNeuron, corrCountsGlia, arraySize, n);

%%
% does neuron and glia occur within the fov?
C = zeros(arraySize, arraySize);
totalN = zeros(arraySize, arraySize);
totalG = zeros(arraySize, arraySize);

for i=1:data.newFrames
   for j=1:arraySize
      for k=1:arraySize
         if corrCountsNeuron(j,k,i)>0     % add total neuron firing (over time)
            totalN(j,k) = totalN(j,k) + corrCountsNeuron(j,k,i); 
            if corrCountsGlia(j,k,i)>0    % neuron and glia fired at the same time.
                C(j,k)=C(j,k)+1;
            end
         end
         if corrCountsGlia(j,k,i)>0       % add total glia events (over time)
             totalG(j,k) = totalG(j,k) + corrCountsGlia(j,k,i);
         end
      end
   end
end
alpha = C./totalN;
alpha(isnan(alpha))=0;

% basic stats for this set of numbers
stats.mean = mean2(alpha);
stats.max = max(alpha(:));
stats.stdev = std2(alpha);
stats.nnz = nnz(alpha);

% print table: j, k, alpha
saveResults(C, totalN, totalG, alpha, stats, n);

disp('Done correlation analysis'); toc;
end

