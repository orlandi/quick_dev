function [out] = gliaAnalysis(data)
% New name
global RUN CONFIG
%save([RUN.dump_path RUN.name '_tmpData'], 'data', 'RUN', 'CONFIG', '-v7.3'); %%% HACK
disp('--------------- GLIA ANALYSIS ----------------------');

%% Generate the neuron mask

neuronsMask = zeros(size(data.frame));
for i = 1:length(data.cells)
    neuronsMask(data.cells(i).mask) = 1;
end
neuronsPixels = find(neuronsMask);

%% Some extra definitions
RUN.gliaUDataFile = [RUN.dump_path RUN.name '_gliaUData.dat'];
RUN.gliaVDataFile = [RUN.dump_path RUN.name '_gliaVData.dat'];
RUN.gliaAverageDataFile = [RUN.dump_path RUN.name '_gliaAverageData.dat'];
RUN.gliaAverageMovieFile = [RUN.video_path RUN.name '_gliaAverageMovie'];
RUN.gliaCompositeMovieFile = [RUN.video_path RUN.name '_gliaCompositeMovie'];

    
%% Now the moving average
num_of_frames_to_average = CONFIG.num_avg_frames;
overlapFrames = floor(CONFIG.num_avg_frames/2);
newFrames = floor((RUN.n_frames - num_of_frames_to_average)/overlapFrames)-1;

astrocitesMovie = zeros(size(data.frame)); 

ct = tic;
%fid = VideoReader(experiment.handle);
if(~CONFIG.preloadAllFrames)
    video = open_movie_videoreader(true);
end
gliaAverageDataFile = fopen(RUN.gliaAverageDataFile, 'w');

textprogressbar('Processing frames for coarse-graining: ');
meanAstrocitesMovie = zeros(size(astrocitesMovie));
% This loop has some overload right now. Each frame is called two times
for i=1:newFrames
    lb = 1+(i-1)*overlapFrames; ub = lb + num_of_frames_to_average;
    sumFrame = zeros(size(astrocitesMovie));
    for pJ=lb:ub
        %currFrame = getFrame(experiment, i, fid);
        if(CONFIG.preloadAllFrames)
            currFrame = data.orgMovie(:, :, pJ);
        else
            currFrame = getFrame(pJ, video);
        end
        sumFrame = sumFrame + double(currFrame);
        
    end
    sumFrame(neuronsPixels) = 0;
    astrocitesMovie = sumFrame./num_of_frames_to_average;
    meanAstrocitesMovie = meanAstrocitesMovie + astrocitesMovie;
    % Store the movie in a raw file to minimize memory usageRUN.dump_path
    fwrite(gliaAverageDataFile, astrocitesMovie, 'double');
    textprogressbar(i/newFrames*100);
end
fclose(gliaAverageDataFile);
meanAstrocitesMovie = meanAstrocitesMovie/newFrames;
textprogressbar(sprintf('Done! (%.2f s)', toc(ct)));

hFig = figure();
set(hFig, 'visible', 'off');
imshow(rescale_image(meanAstrocitesMovie));
savefig(RUN.name, 'meanAstrocitesMovie');
close(hFig);
if(CONFIG.useLUTcorrection)
    [minIntensity, maxIntensity] = autoLevelsFIJI(meanAstrocitesMovie, RUN.bitDepth);
    RUN.avgFrameMinIntensity = minIntensity;
    RUN.avgFrameMaxIntensity = maxIntensity;
    hFig = figure();
    set(hFig, 'visible', 'off');
    imshow(meanAstrocitesMovie, 'DisplayRange', [minIntensity maxIntensity]);
    savefig(RUN.name, 'meanAstrocitesMovieCorrected');
    close(hFig);
end

if(~CONFIG.preloadAllFrames)
    close_movie_videoreader(video);
    clear video;
end

fprintf('Corse-grain level: %d frames/newFrame. Total frames used: %d/%d\n', num_of_frames_to_average, ub, RUN.n_frames);


%% Let's save this movie - just in case
f_path = [RUN.video_path 'astrocyte' filesep];
if ~exist(f_path, 'file'), mkdir(f_path);end;
newMovie = VideoWriter(RUN.gliaAverageMovieFile, 'Grayscale AVI');
open(newMovie);
gliaAverageDataFile = fopen(RUN.gliaAverageDataFile, 'r');
for i = 1:newFrames
    astrocitesMovie = fread(gliaAverageDataFile, size(data.frame), 'double');
    if(CONFIG.useLUTcorrection)
        rescaledFrame = (astrocitesMovie-minIntensity)/(maxIntensity-minIntensity);
        rescaledFrame(rescaledFrame > 1) = 1;
        rescaledFrame(rescaledFrame < 0) = 0;
    else
        rescaledFrame = rescale_image(astrocitesMovie);
    end
    writeVideo(newMovie, rescaledFrame);
end
close(newMovie);
fclose(gliaAverageDataFile);

%% OPTICAL FLOW (Horn-Schunck method)
% Probably better to just use fwrite and fread to store u,v (r, theta) and Intensity
% data
maxI = newFrames-1;

kernelChoice = {'average','kernel1'};
imSize = size(data.frame,1);

gliaUDataFile = fopen(RUN.gliaUDataFile, 'w');
gliaVDataFile = fopen(RUN.gliaVDataFile, 'w');
gliaAverageDataFile = fopen(RUN.gliaAverageDataFile, 'r');
% currently only doing kernel_1 filter
for nVid=2:size(kernelChoice,2)
    
    localMaxMag = zeros(1, maxI);
    uPast = zeros(size(data.frame));
    vPast = zeros(size(data.frame));
    avgU = zeros(size(data.frame));
    avgV = zeros(size(data.frame));
    
    tic;
    % initial calculation for the first u and v values.
    astrocitesMoviePast = fread(gliaAverageDataFile, size(data.frame), 'double'); % Read the first frame
    astrocitesMovieNow = fread(gliaAverageDataFile, size(data.frame), 'double'); % Read the second frame
    [uNow, vNow] = HS(astrocitesMoviePast, ...
        astrocitesMovieNow, 5, 100, uPast, vPast, kernelChoice{nVid});
    avgU = avgU + uNow;
    avgV = avgV + uNow;
    mag = sqrt(uNow.^2 + vNow.^2);
    localMaxMag(1) = max(mag(:));
    % Store the components
    fwrite(gliaUDataFile, uNow, 'double');
    fwrite(gliaVDataFile, vNow, 'double');
    
    % calculate the rest of the velocity vectors for each frame
    ct = tic;
    textprogressbar('Calculating 0.F: ');
    for i=2:maxI
        % Cycle the data
        uPast = uNow;
        vPast = vNow;
        astrocitesMoviePast = astrocitesMovieNow;
        astrocitesMovieNow = fread(gliaAverageDataFile, size(data.frame), 'double'); % Read the second frame
        [uNow, vNow] = HS(astrocitesMoviePast, ...
            astrocitesMovieNow, 5, 100, uPast, vPast, kernelChoice{nVid});
        avgU = avgU + uNow;
        avgV = avgV + uNow;
        mag = sqrt(uNow.^2 + vNow.^2);
        localMaxMag(i) = max(mag(:));
        % Store the components
        fwrite(gliaUDataFile, uNow, 'double');
        fwrite(gliaVDataFile, vNow, 'double');
    
        textprogressbar((i-1)/(maxI-1)*100);
    end
    avgU = avgU/maxI;
    avgV = avgV/maxI;
    avgMag = sqrt(avgU.^2 + avgV.^2);
    globalMaxMag = max(localMaxMag);
    fclose(gliaUDataFile);
    fclose(gliaVDataFile);
    fclose(gliaAverageDataFile);
    textprogressbar(sprintf('Done! (%.2f s)', toc(ct)));
  
    %%% Construct information about the neurons firing
    firingFrames = sortNeurons(data);
    
    %%% Plotting
    if CONFIG.create_vid
        gliaUDataFile = fopen(RUN.gliaUDataFile, 'r');
        gliaVDataFile = fopen(RUN.gliaVDataFile, 'r');
        gliaAverageDataFile = fopen(RUN.gliaAverageDataFile, 'r');

        disp('Plotting and making video...');
        
        %--- structure for setting tiff tags
        %tagstruct.Photometric = Tiff.Photometric.RGB;
        %tagstruct.BitsPerSample = 8;
        %tagstruct.SamplesPerPixel = 3;
        %tagstruct.PlanarConfiguration = Tiff.PlanarConfiguration.Chunky;
        
        str = strcat(RUN.video_path, RUN.name, '_AstrocyteFlowC_', kernelChoice{nVid}, '_', num2str(CONFIG.N));
        newMovie = VideoWriter(str, 'Uncompressed AVI');
        open(newMovie);
        %t = Tiff(str, 'w');
        
        ct = tic;
        textprogressbar('Recording optical flow video: ');
        for i=1:maxI
            u = fread(gliaUDataFile, size(data.frame), 'double');
            v = fread(gliaVDataFile, size(data.frame), 'double');

            astrocitesMovie = fread(gliaAverageDataFile, size(data.frame), 'double');
            [~, hFig] = plotFlow2(u, v, globalMaxMag, ...
                astrocitesMovie, 5 , 3);
            frame = getframe(hFig);

            %-- tiff storage tags
            %tagstruct.ImageLength = size(frame.cdata,1);
            %tagstruct.ImageWidth = size(frame.cdata,2);
            %t.setTag(tagstruct);
            %t.write(frame.cdata);
            %writeDirectory(t);
            writeVideo(newMovie, frame.cdata);
            
            delete(hFig);

            textprogressbar(i/maxI*100);
        end
        textprogressbar(sprintf('Done! (%.2f s)', toc(ct)));
        %t.close();
        close(newMovie);
    end
    
    data.localMaxMag = localMaxMag;
    data.globalMaxMag = globalMaxMag; data.firingFrames = firingFrames;
    data.avgFrameRate = num_of_frames_to_average;
    mag = sqrt(u.^2+v.^2);
    clear u;
    clear v;
    data.mag = mag;
    clear mag;
    
    plotFlow2(avgU, avgV, max(avgMag(:)), data.frame, 1, 3);
    n_fig =[ 'Average_OF_' num2str(CONFIG.N)];
    saveGlialFig(gcf, n_fig);
    
    data.gliaUDataFile = RUN.gliaUDataFile;
    data.gliaVDataFile = RUN.gliaVDataFile;
    data.gliaAverageDataFile = RUN.gliaAverageDataFile;
    data.newFrames = maxI;
    createGliaFlowCompositeMovie(data);
    %%% Analyze (not done)
    [ glialEvents ] = correlationAnalysis(data);
    data.glialEvents = glialEvents;
    
end

% We should delete the dump files - or compress them
delete(RUN.gliaUDataFile);
delete(RUN.gliaVDataFile);
delete(RUN.gliaAverageDataFile);

out = data;

end


