function [PlotImage] = neuron_mask_no_label(cells, img)
% get the neuron mask but this time don't label the figure. 

    allmasks = false(size(img));
    for i=1:length(cells)
        allmasks = or(allmasks, cells(i).mask);
    end
    
    %--- show_image_and_masks(cells, img) function below
    I = rescale_image(img);
    mask = single(allmasks);
    invmask = single(not(allmasks));
    PlotImage = cat(3, ...
            mask .* ones(size(I)), ...
            invmask .* I, ...
            invmask .* I);
    figure
    imshow(PlotImage, 'InitialMagnification', 'fit');
    title('Neuron mask');
    close;
end