function [ glia_events ] = get_glia( frame )
%GET_GLIA: finds the "glial cells" in each frame. The goal is to establish
%the area and location of possible glial events. 
%   frame: a particular frame in predictedEvents.

global CONFIG

dbg=CONFIG.debug;

% find cells
CC = bwconncomp(frame, 4);
PROPS = regionprops(CC, 'Solidity', 'Centroid', 'Area', 'BoundingBox');

glia_events = [];
% area size to ignore
event_maxArea = CONFIG.glial_maxArea_threshold;
event_minArea = CONFIG.glial_minArea_threshold;

for i=1:CC.NumObjects
%     waitbar(i/CC.NumObjects);
    
    cc = CC.PixelIdxList{i};
    props = PROPS(i);
    
    single_cell = 1;
    if props.Solidity < 0.9
        if dbg, fprintf('object %d is potentially more than one cell...\n', i);end;
        
        % we will need to work with the base frame
        img = frame;
        
        % mask out everything else
        BW=xor(img,img);
        BW(cc)=1;
        img(~BW)=0;
        
        % find the components for only this image
        CC_m = bwconncomp(img > median(img(BW)), 4);
        
        
        % if we indeed have more than one cell
        if CC_m.NumObjects > 1
            PROPS_m = regionprops(CC_m, ...
                'Solidity', 'Centroid', 'Area', 'BoundingBox');
            single_cell = 0;
            if dbg, fprintf('\tTrue, spliting in %d cells.\n', CC_m.NumObjects);end;
            for k=1:CC_m.NumObjects
                cc = CC_m.PixelIdxList{k};
                props = PROPS_m(k);
                if props.Area > event_maxArea || props.Area < event_minArea
                    if dbg, fprintf('\tnot using split %d of cell %d\n',k,i);end;
                    continue
                end
                glia_events = [glia_events;create_cell(cc, props, frame)];
            end % new cells
        end % more than one cells
    end % possible more than one cell
    
    % skip 
    if props.Area > event_maxArea || props.Area < event_minArea
        if dbg, fprintf('ignoring cell %d\n',i);end;
        continue
    end
    
    if single_cell
        glia_events = [glia_events;create_cell(cc, props, frame)];
    end
end
end

function c = create_cell(cc, props, frame)
mask = xor(frame, frame);
mask(cc) = 1;

c = struct(...
    'center', props.Centroid(2:-1:1), ...
    'mask', find(mask), ...
    'area', sum(mask(:)), ...
    'bounding_box', props.BoundingBox);
end

