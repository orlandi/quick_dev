function appendData( PathName, FileName, savePath, saveFileName)
%APPENDDATA Summary of this function goes here
%   Detailed explanation goes here

fmt1 = '%8.4f\t%8.4f\t%8.4f\t%8.4f\t%8.4f\t%8.4f\n';        %format for regular data 
fmt2 = '%8.4f\t%8.4f\t%8.4f\t%8.4f\t%8.4f\t%8.4f\t%8.4f\t%8.4f\t%8.4f\t%8.4f\n';        % first line data

datafid = fopen(fullfile(PathName, FileName), 'r');
fgets(datafid); %skip over string titles

line1 = fgets(datafid);
line1 = str2num(line1);

data = textscan(datafid, fmt1);
fclose(datafid);

%open save location
fileID = fopen([savePath filesep saveFileName], 'a+');
fprintf(fileID, fmt2, ...
    line1(1), line1(2), line1(3), line1(4), line1(5), line1(6), line1(7), line1(8), line1(9), line1(10));

for i=1:size(data{1},1)
    fprintf(fileID, fmt1, ...
        data{1}(i), data{2}(i), data{3}(i), data{4}(i), data{5}(i), data{6}(i));
end

fprintf(fileID, '\n\n');
fclose(fileID);

disp('Append Complete.');
end

