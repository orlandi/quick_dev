function [ newPathName, newFileName ] = getNewPath( PathName, FileName )
% look for other parts of the same movie

k1 = strfind(FileName, '-1');
k2 = strfind(FileName, '-2');
k3 = strfind(FileName, '-3');

if ~isempty(k1) && isempty(k2) && isempty(k3)      % opened p1
    disp('Opened p1, looking for p2...');
    
    newPathName = strrep(PathName, '\p1', '\p2');  % now look for p2
    newFileName = strrep(FileName, '-1', '-2');
    
    if ~exist(newPathName, 'dir')               % p2 DNE, find p3
        disp('p2 not found, looking for p3...');
        
        newPathName = strrep(newPathName, '\p2', '\p3');
        newFileName = strrep(newFileName, '-2', '-3');
        
        if ~exist(newPathName, 'dir')
            disp('Nothing was found');
            
            newPathName = '';
            newFileName = '';
        end
    end
elseif ~isempty(k2) && isempty(k1) && isempty(k3)  % opened p2
    disp('Opened p2, looking for p3...');
    
	newPathName = strrep(PathName, '\p2', '\p3');  % now look for p3
    newFileName = strrep(FileName, '-2', '-3');
    
    if ~exist(newPathName, 'dir')                % p3 DNE
        disp('Nothing was found');
        
        newPathName = '';
        newFileName = '';
    end
elseif ~isempty(k3) && isempty(k2) && isempty(k1)  % opened p3
    disp('Opened p3. Finish.');
    
    newPathName='';                              % finish.
    newFileName='';
end

end

