run = 1;

while run>0
    % select text file
    extension='D:\LindenData\*.txt';
    label='Select movie file';
    [FileName, PathName] = uigetfile(extension, label);
    if FileName ==0
        run=0;
        break;
    end

    if run==1       % ask for saving location
        extension = 'D:\LindenData\';
        savePath = uigetdir(extension, 'Save location');
        run=2;
    end
    
    % ask for new file name
    saveFileName = input('New file name is? ', 's');
    
    % append current data
	appendData(PathName, FileName, savePath, saveFileName);
    
    % grab all other parts
    while run>0
        [PathName, FileName] = getNewPath(PathName, FileName);
        if ~isempty(PathName)        % repeat load files
            appendData(PathName, FileName, savePath, saveFileName);
        else
            break;
        end
    end
end

disp('End.');
