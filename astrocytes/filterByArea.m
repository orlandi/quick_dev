function [ filteredEvents ] = filterByArea(eventsFileName, n, frame, Nframes )
%FILTERBYAREA: Remove predicted glial events based on the glia contour
%size. If the size is too big, most likely it is not an incident event but
%simply the propagation of a glial firing that occurred previously. If the
%size is too small, it may just be noise during the recording. 
%   eventsFileName: a vector of frames that contains the information of
%   the predicted glial incident events. 
%   n: a number to keep track of output files. 
%  frame, Nframes (for the sizes)

global CONFIG
warning('off', 'MATLAB:contour:ConstantData');
%eventsFile = predictedEvents;
filteredEvents = cell(1,Nframes);
incidentEventFile = fopen(eventsFileName, 'r');
ct = tic;
textprogressbar('Filtering glial events by area: ');
for t=1:length(filteredEvents)
    predictedEvents = fread(incidentEventFile, size(frame), 'double');
    filteredEvents{t} = get_glia(predictedEvents); %filteredEvents is of type 'cell'
    textprogressbar(t/length(filteredEvents)*100);
end
textprogressbar(sprintf('Done! (%.2f s)', toc(ct)));
fclose(incidentEventFile);
%%
% 1st plot: predicted glial events
figSize = size(frame, 1);
hFig = figure();
hold on;
set(hFig, 'visible', 'off');
axis([0 figSize 0 figSize]);
incidentEventFile = fopen(eventsFileName, 'r');
ct = tic;
textprogressbar('Plotting predicted glial events: ');
invalidEvents = 0;
for t=1:Nframes
    predictedEvents = fread(incidentEventFile, size(frame), 'double');
    if(~all(predictedEvents(:) == predictedEvents(1)))
        contour(predictedEvents);
    else
        invalidEvents = invalidEvents + 1;
    end
    textprogressbar(t/length(filteredEvents)*100);
end
fclose(incidentEventFile);
title('Predicted Glial Incident Events'); hold off;
globalT = strrep(num2str(CONFIG.glial_globalI_threshold), '.', '');
localT = strrep(num2str(CONFIG.glial_localI_threshold), '.', '');
n_fig = strcat('predicted_', globalT,'_', localT, '_', n);
saveGlialFig(hFig, n_fig);
textprogressbar(sprintf('Done! (%.2f s)', toc(ct)));
if(invalidEvents > 0)
    fprintf('Looks like %.2f%% of the contour plots contained constant data\n', invalidEvents/length(filteredEvents)*100);
end

% 2nd plot: glial incident events after the area filter. 
hFig = figure();
hold on;
set(hFig, 'visible', 'off');
axis([0 figSize 0 figSize]);
ct = tic;
textprogressbar('Plotting glial incident events after the area filter: ');
for t=1:Nframes
    for i=1:length(filteredEvents{t})
        scatter(filteredEvents{t}(i).center(2), filteredEvents{t}(i).center(1), 10, [0.0,0.0,0.0], 'filled');
    end
    textprogressbar(t/length(filteredEvents)*100);
end
areaT = num2str(CONFIG.glial_maxArea_threshold);
n_fig = strcat('filterByArea_', areaT, '_', n);
saveGlialFig(hFig, n_fig);
textprogressbar(sprintf('Done! (%.2f s)', toc(ct)));

% 3rd plot: superposition of plot 1 and plot 2. 
hFig = figure();
set(hFig, 'visible', 'off');
hold on;
axis([0 figSize 0 figSize]);
incidentEventFile = fopen(eventsFileName, 'r');
ct = tic;
textprogressbar('Plotting the superposition of the previous plots: ');
for t=1:Nframes
    predictedEvents = fread(incidentEventFile, size(frame), 'double');
    if(~all(predictedEvents(:) == predictedEvents(1)))
        contour(predictedEvents);
    end
    for i=1:length(filteredEvents{t})
        scatter(filteredEvents{t}(i).center(2), filteredEvents{t}(i).center(1), 10, [0.0,0.0,0.0], 'filled');
    end
    textprogressbar(t/length(filteredEvents)*100);
end

fclose(incidentEventFile);
title('Superposition of Filtered and Predicted Glial Incident Events'); hold off;
n_fig = strcat('predicted_vs_filtered_glial_events_comparison_', n);
saveGlialFig(hFig, n_fig);
textprogressbar(sprintf('Done! (%.2f s)', toc(ct)));

warning('on', 'MATLAB:contour:ConstantData');
end

