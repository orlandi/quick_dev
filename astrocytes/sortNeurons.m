function [out] = sortNeurons( data )
% SORTNEURONS: given the neuron firings found previously, this function
% will re-organize that information depending on the frame that the firing
% occurs. (Previously it was stored based on the neuron number).
%   data: the main structure that contains all the information we have
%   extracted from the videos so far. Hence, it contains the information on
%   when each neuron fires. 
%   out: sorted list of neuron firings based on which frames the firing
%   occured. 

global RUN

frame = struct('x', [], 'y', []);

% matlab weirdness thing... 
for i=1:RUN.n_frames
    frame(i).x = [];
    frame(i).y = [];
end

%% for each cell, find the frame which it fires at
for cellNum=1:size(data.peak_locations,2)
    if ~isempty(data.peak_locations{cellNum}) %not empty, store cell location into frame
        
        %firing cell location
        xPos = data.cells(cellNum).center(1);
        yPos = data.cells(cellNum).center(2);
        
        for firingNum = 1:size(data.peak_locations{cellNum},2)
            frameNum = data.peak_locations{cellNum}(firingNum);
            
            %append x,y location
            index = size(frame(frameNum).x,2)+1;
            
            frame(frameNum).x(index) = xPos;
            frame(frameNum).y(index) = yPos;
        end
    end
end

out = frame;
end