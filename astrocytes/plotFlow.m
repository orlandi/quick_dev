function plotFlow( u, v, mag, maxmag, imgOriginal, rSize, scale )
% Creates a quiver plot that displays the optical flow vectors on the
% original first frame (if provided). See the MATLAB Function Reference for
% "quiver" for more info.
%
% Usage:
% plotFlow(u, v, imgOriginal, rSize, scale)
%
% u and v are the horizontal and vertical optical flow vectors,
% respectively. imgOriginal, if supplied, is the first frame on which the
% flow vectors would be plotted. use an empty matrix '[]' for no image.
% rSize is the size of the region in which one vector is visible. scale
% over-rules the auto scaling.
%
% Author: Mohd Kharbat at Cranfield Defence and Security
% mkharbat(at)ieee(dot)org , http://mohd.kharbat.com
% Published under a Creative Commons Attribution-Non-Commercial-Share Alike
% 3.0 Unported Licence http://creativecommons.org/licenses/by-nc-sa/3.0/
%
% October 2008
% Rev: Jan 2009

figure();

if nargin>2
    if sum(sum(imgOriginal))~=0
        imshow(imgOriginal,[0 255]);
        hold on;
    end
end
if nargin<4
    rSize=5;
end
if nargin<5
    scale=3;
end

% Enhance the quiver plot visually by showing one vector per region
for i=1:size(u,1)
    for j=1:size(u,2)
        if floor(i/rSize)~=i/rSize || floor(j/rSize)~=j/rSize
            u(i,j)=0;
            v(i,j)=0;
        end
    end
end

%% repeat for each color
% Want a colored quiver plot so we can see which vectors has a higher
% magnitude. To do this, the vectors for each frame is divided into some
% threshold ranges given by the variable: mag_range. The colors for each
% range is given in colors. 
%   Ex:// plot vectors as blue if the magnitude of the vector is between 0
%   and 0.1*maximum_magnitude. 
mag_range = [0.0, 0.1, 0.20, 0.30, 0.50, 0.7, 1];
number_plots = size(mag_range,2) - 1;
colors = { ...
%     [0.0,0.0,0.0],...   %BLACK
    [0.0,0.0,1.0], ...  %BLUE
    [0.0,1.0,1.0], ...  %CYAN
	[0.0,1.0,0.0], ...  %GREEN
    [1.0,1.0,0.0], ...  %YELLOW
    [0.91, 0.41, 0.17], ... %ORANGE
    [1.0,0.0,0.0], ...  %RED
    };
for plot_count = 1:number_plots
    mag_low = maxmag*mag_range(plot_count);
    mag_high = maxmag*mag_range(plot_count+1);
    
    uplot = gpuArray(u);            % to speed things up, we're going to plot using GPU. 
    uplot(mag<=mag_low) = 0;        % temporarily set other vectors to 0. 
    uplot(mag>mag_high) = 0;
    
    vplot = gpuArray(v);
    vplot(mag<=mag_low) = 0;
    vplot(mag>mag_high) = 0;
    
    quiver(uplot, vplot, scale, 'Color',colors{plot_count});
    hold on;    % still gotta plot the other colors.
end

set(gcf,'units','normalized','outerposition',[0 0 1 1]);    % "fullscreen"
set(gca,'YDir','reverse');
hold off;

end

