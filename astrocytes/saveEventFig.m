function saveEventFig(corrCountsNeuron, corrCountsGlia, arraySize, n )
%PLOT_EVENTS Summary of this function goes here
%   Detailed explanation goes here
global RUN

% plotting collected data
% X = zeros(1, arraySize^2);
% Y = zeros(1, arraySize^2);
% Z = zeros(1, arraySize^2);
% neuronSize = zeros(1, arraySize^2); % these are the counts
% gliaSize = zeros(1, arraySize^2);

hFig = figure();
set(hFig, 'visible', 'off');
% for t=1:size(corrCountsNeuron,3)
%    it = 1;
%     for iX=1:arraySize
%         for iY=1:arraySize
%             X(it)=iX;
%             Y(it)=iY;
%             Z(it)=t;
%             neuronSize(it) = corrCountsNeuron(iX, iY, t)*10+0.1;
%             gliaSize(it) = corrCountsGlia(iX, iY, t)*10+0.1;
%             
%             it = it+1;
%         end
%     end
%     scatter3(X, Y, Z, neuronSize, 'r', 'filled'); hold on;
%     scatter3(X, Y, Z, gliaSize, 'b', 'filled');
% end
[X,Y,Z] = meshgrid(1:size(corrCountsNeuron,1), 1:size(corrCountsNeuron,2), 1:size(corrCountsNeuron,3));
validNeuron = find(corrCountsNeuron);
validGlia = find(corrCountsGlia);
scatter3(X(validNeuron), Y(validNeuron), Z(validNeuron), corrCountsNeuron(validNeuron)*10+0.1, 'r', 'filled');
hold on;
scatter3(X(validGlia), Y(validGlia), Z(validGlia), corrCountsGlia(validGlia)*10+0.1, 'b', 'filled');

hold off;
title('Comparison of Glial (B) versus Neuron (R) Events');
xlabel('X position bucket');
ylabel('Y position bucket');
zlabel('Average time');

n_fig = strcat('GlialNeuronEventsComparison_', n);
n_data = RUN.name;
f_path = [RUN.figure_path 'astrocyte' filesep];
if ~exist(f_path, 'file'), mkdir(f_path);end;

saveas( hFig, [f_path n_data '_' n_fig '.png']);

close(hFig);
end

