function saveGlialFig( hFig, n_fig)
%SAVEGLIALFIG Summary of this function goes here
%   Detailed explanation goes here

global RUN
n_data = RUN.name;

f_path = [RUN.figure_path 'astrocyte' filesep];
if ~exist(f_path, 'file'), mkdir(f_path);end;

saveas( hFig, [f_path n_data '_' n_fig '.png']);

close(hFig);
end

