figure;
for i=1:length(d.burst_time) % all regions
    for j=1:length(d.burst_time{i}) % all bursts
        cr = d.burst_cells{i}{j}; % recruited cells
        centers = reshape([data.cells(cr).center], 2, size(cr, 1));
        X = centers(1,:);
        Y = centers(2,:);

        scatter(X, Y);%, d.burst_time{i}(j));
        ylim([0 480]);
        xlim([0 640]);
        
        pause(0.3);
    end % all bursts
end % all regions
%%view(30, 100);
