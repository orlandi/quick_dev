function [ work_function, setup_data, data_name ] = get_trace_gpu( frame, cells )
%[work_function, setup_data, data_name] = get_trace( frame, cells )
% Get's traces for each cell.
% setup_data contains one mask for each cell.
% work_function will return an array, one averarege pixel value per cell
% data_name is 'trace'

global CONFIG
if CONFIG.debug,   disp('Preparing get_trace_gpu');end;

work_function = @get_trace_worker;


k = parallel.gpu.CUDAKernel(...
    'get_traces.ptx', ...
    'get_traces.cu', ...
    'cuda_get_traces');
blocksize=[16 16];
k.ThreadBlockSize = blocksize;
k.GridSize = round(size(frame) ./ blocksize);

setup_data = {...
    zeros(size(frame), 'uint32'),...
    zeros(size(cells), 'double'),...
    k};

for i=1:length(cells)
    setup_data{1}(cells(i).mask) = i;
    setup_data{2}(i) = 1 ./ sum(cells(i).mask(:));
end

setup_data{1} = gpuArray(uint32(setup_data{1}));

data_name = 'trace';
end

function traces = get_trace_worker(frame, sd)
traces = zeros(size(sd{2}), 'uint32');
f = gpuArray(uint32(frame));
traces = gather(feval(sd{3}, f, sd{1}, traces));
traces = double(traces) .* sd{2};
clear f;
end
