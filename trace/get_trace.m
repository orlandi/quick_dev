function [ work_function, setup_data, data_name ] = get_trace( frame, cells )
%[work_function, setup_data, data_name] = get_trace( frame, cells )
% Get's traces for each cell.
% setup_data contains one mask for each cell.
% work_function will return an array, one averarege pixel value per cell
% data_name is 'trace'

global CONFIG
if CONFIG.debug,   disp('Preparing get_trace');end;

work_function = @get_trace_worker;

setup_data = {};
for i=1:length(cells)
    setup_data{i} = cells(i).mask;
end

data_name = 'trace';
end

function traces = get_trace_worker(frame, sd)
traces = zeros([length(sd) 1]);
for i=1:length(sd)
    traces(i) = mean(frame(sd{i}));
end

end