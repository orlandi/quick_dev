/**
  Gets the traces from frame, using the cell ids contained in idx, outputing trace
  sums in traces
  */

//extern "C" {
__global__ void cuda_get_traces(
        const unsigned int* frame, 
        const unsigned int* idx, 
        unsigned int* traces) {
    // gets the image position
    unsigned int i = blockIdx.x * blockDim.x + threadIdx.x;
    unsigned int j = blockIdx.y * blockDim.y + threadIdx.y;
    unsigned int k = j*(blockDim.x*gridDim.x)+i;

    const unsigned int cell_idx = idx[k];
    
    if (cell_idx > 0) {
        atomicAdd(&traces[cell_idx - 1], frame[k]);
    }
}
//}
