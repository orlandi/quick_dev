function [ data ] = find_firing( in_data )
%FIND_FIRING Summary of this function goes here: 
%Looks at peaks that were found previously and if the peak value passes the
% required threshhold, then a firing has occured. Record this firing. 
%   Detailed explanation goes here
%   

data=in_data;

global RUN CONFIG

if RUN.from_image
    return
end

% data values setup
% data.num_peaks          = total number of peaks for each cell
% data.num_events         = number of firing events per frame
% data.peak_locations     = time of each frame containing firing events

tic;
peak_locations = {};
num_events = zeros(size(data.trace), 'uint8');
num_peaks = zeros(size(data.trace, 1), 1, 'uint32');

oopsi_pk = data.oopsi_peak_values;
indexes = 1:size(oopsi_pk, 2); % This wasn't right

for i=1:size(oopsi_pk, 1)
    pk = oopsi_pk(i,:);
    peak_threshold = CONFIG.oopsi_peak_threshold;%max(0.5, 5*std(pk));
    pk_vls = floor(pk - peak_threshold) + 1;
    pks = find(pk_vls > 0);
    
    % remove events too near region borders
    delta = 1*30; % 1 second
    for r=1:size(data.regions, 2)
        range = data.regions(:,r);
        pks(abs(pks-range(1))<delta | abs(pks-range(2))<delta)=[];
    end;
    pk_vls(~ismember(indexes, pks)) = 0;
    peak_locations{i} = pks;
    
    num_peaks(i)            = length(peak_locations{i});
    num_events(i, :)        = pk_vls;                       % *** ?
end

data.num_peaks          = num_peaks;
data.num_events         = num_events;
data.peak_locations     = peak_locations;
fprintf('found firing events in %.2f s\n', toc);
end