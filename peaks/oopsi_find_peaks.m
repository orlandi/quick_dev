function [pks] = oopsi_find_peaks(F, varargin)
if(nargin == 2)
    V = varargin{1};
else
    % Default values in case we do not pass any argument
    V=struct;
    V.dt=1/30;
    V.fast_iter_max = 15;%15;
    V.fast_poiss = 0; %1=poisson/0=gaussian
    V.fast_thr = 0;
    V.plot = 0;
    V.est_a = 1;
    V.est_b = 1;
end

% preprocess
V.T     = length(F);
F       = preprocess_traces(F);

P=struct;
% infer spikes
warning('off','MATLAB:rankDeficientMatrix');
[pks, ~] = fast_oopsi(F,V,P);
warning('on','MATLAB:rankDeficientMatrix');
end