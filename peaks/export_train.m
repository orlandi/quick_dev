function export_train( fout, data )
%EXPORT_TRAIN export spike train data.

% header
fprintf(fout, '##\n##firing events data\n##\n');

% cells
fprintf(fout,'#num_events;ev1,ev2,ev3,....\n');
fprintf(fout, '#firing_events\n');
for i=1:length(data.peak_locations)
    fprintf(fout, '%d;',data.num_peaks(i));
    train = data.peak_locations{i};
    for j=1:data.num_peaks(i)
        if j>1
            fprintf(fout, ',');
        end
        fprintf(fout, '%d', train(j));
    end
    fprintf(fout, '\n');
end
end