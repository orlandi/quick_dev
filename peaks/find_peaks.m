function data = find_peaks(in_data)
% data=find_peaks(data)
% uses the 'trace' member of data, to find all the firing events using 
% OOPSI.
% 
% Fills out the following data members
% data.oopsi_peak_values OOPSI "peak" value for each frame, for each cell

data=in_data;

global RUN
if RUN.from_image
    return
end

trun = tic;
data = in_data;
traces = data.trace;

fprintf('Running oopsi.\n');
t_oopsi = tic;

oopsi_peak_values = find_the_peaks(traces, data.regions); %*** 

data.oopsi_peak_values  = oopsi_peak_values;
fprintf('done running oopsi in %.2f seconds.\n', toc(t_oopsi));
fprintf('full find_peaks run in %.2f seconds.\n', toc(trun));
end

%%%%%%%%%%%
function [oopsi_peak_values] = find_the_peaks(traces, regions)
oopsi_peak_values = zeros(size(traces));
global CONFIG RUN
dbg=CONFIG.debug;

if(CONFIG.useParallelization)
    parforArg = inf;
else
    parforArg = 0;
end

% figure out work partitioning
cell_count = size(traces, 1);
cells_per_lab = CONFIG.cells_per_lab;
n_split = ceil(cell_count/cells_per_lab);
if dbg, fprintf('Working on %d cells, sending %d to each worker (%d batches)\n',cell_count,cells_per_lab,n_split);end;
tmp_peak_values = cell(n_split, 1);

if dbg;calc_tic=tic;end;
% Let's configure the oopsi structure here
V=struct;
V.dt= 1/RUN.fps;
V.fast_iter_max = 15;%15;
V.fast_poiss = 0; %1=poisson/0=gaussian
V.fast_thr = 0;
V.plot = 0;
V.est_a = 1;
V.est_b = 1;
    
parfor (outer=1:n_split, parforArg)
    if dbg;tic;end;

    first = (outer-1)*cells_per_lab+1;
    last = min(cell_count,outer*cells_per_lab);

        %takes a chunk of data.trace 
    bulk_tr = traces(first:last,:);     
    bulk_pks = zeros(size(bulk_tr));

    if dbg;fprintf('starting outer %d on cells %d to %d\n',outer,first,last);end;

    for i=1:size(bulk_tr, 1)
        tr = bulk_tr(i,:);
        pl = {};

        l = size(regions, 2);
        all_pks = [];
        for p=1:l
            a = regions(1,p);b=regions(2,p);
            tt=tr(a:b);
            f = zeros(length(tt),1, class(tt));
            % skip even and small regions
            if mod(p, 2) ~= 0 && (b-a)>3*30
                f = oopsi_find_peaks(tt, V);
            end

            pl{p} = f;
            all_pks = [all_pks;f];
        end

        bulk_pks(i,:) = all_pks;
    end
    tmp_peak_values{outer} = bulk_pks;

    if dbg;fprintf('outer %d took %.2fs to run\n',outer, toc);end
end % for all cells
if dbg;fprintf('took %.2 to calculate all peaks\n',toc(calc_tic));end

if dbg;tic;end;
for outer=1:n_split
    a = (outer-1)*cells_per_lab+1;
	b = min(cell_count,(outer*cells_per_lab));
    oopsi_peak_values(a:b,:) = tmp_peak_values{outer};
end

if dbg;fprintf('took %.2f to gather up results\n', toc);end;
end
