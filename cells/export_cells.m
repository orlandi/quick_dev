function export_cells( fout, data )
%EXPORT_CELLS exports information about the detected cells

% header
fprintf(fout, '##\n##cells data\n##\n');

fprintf(fout,'#center_x,center_y;area;bbox_x0,bbox_y0,bbos_width,bbox_height\n');
fprintf(fout,'#cells\n');

for i=1:length(data.cells)
    c = data.cells(i);
    fprintf(fout, '%5.1f,%5.1f;%d;%d,%d,%d,%d\n',...
        c.center(1), c.center(2),...
        c.area,...
        ceil(c.bounding_box(1)), ceil(c.bounding_box(2)),...
        c.bounding_box(3), c.bounding_box(4));
    
end % for
end % function
