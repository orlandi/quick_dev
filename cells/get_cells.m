function [ cells ] = get_cells(frame)
%GET_CELLS gets the position and mask of each cell in the frame.
%   [cells] = get_cells(frame)
%   Extracts cell information from the average frame. Search for 
%   boundaries in the image, and constructs a mask for each boundary.
% frame = select_soma(frame);

global CONFIG

dbg=CONFIG.debug;
% remove background and threshold
no_back = frame - ...
    imopen(frame, strel('ball', background_size(), background_size(), 0));
no_back_bw = imopen(no_back > 1, strel('disk', 1, 0));

% find cells
CC = bwconncomp(no_back_bw, 4);
PROPS = regionprops(CC, 'Solidity', 'Centroid', 'Area', 'BoundingBox');
ct = tic;
textprogressbar('Finding cells: ');
cells = [];
% area size to ignore
cell_area_ignore = max(9, (background_size()/4-1)^2);

for i=1:CC.NumObjects
    textprogressbar(i/CC.NumObjects*100);

    cc = CC.PixelIdxList{i};
    props = PROPS(i);

    single_cell = 1;
    if props.Solidity < 0.9
        if dbg, fprintf('object %d is potentially more than one cell...\n', i);end;

        % we will need to work with the base frame
        img = frame;

        % mask out everything else
        BW=xor(img,img);
        BW(cc)=1;
        img(~BW)=0;

        % find the components for only this image
        CC_m = bwconncomp(img > median(img(BW)), 4);

        % if we indeed have more than one cell
        if CC_m.NumObjects > 1
            PROPS_m = regionprops(CC_m, ...
                'Solidity', 'Centroid', 'Area', 'BoundingBox');
            single_cell = 0;
            if dbg, fprintf('\tTrue, spliting in %d cells.\n', CC_m.NumObjects);end;
            for k=1:CC_m.NumObjects
                cc = CC_m.PixelIdxList{k};
                props = PROPS_m(k);
                if props.Area < cell_area_ignore
                    if dbg, fprintf('\tnot using split %d of cell %d\n',k,i);end;
                    continue
                end
                cells = [cells;create_cell(cc, props, frame)];
            end % new cells
        end % more than one cells
    end % possible more than one cell

    % skip if we are not big enough
    if props.Area < cell_area_ignore
        if dbg, fprintf('ignoring cell %d\n',i);end;
        continue
    end

    if single_cell
        cells = [cells;create_cell(cc, props, frame)];
    end
end
textprogressbar(sprintf('Done! (%.2f s)', toc(ct)));
end
% There is no need to store the whole mask. Just pixel indexes. No idea on
% what's going to happen with GPU trace extraction tho
function c = create_cell(cc, props, frame)
    mask = xor(frame, frame);
    mask(cc) = 1;
    
    c = struct(...
        'center', props.Centroid(2:-1:1), ...
        'mask', find(mask), ...
        'area', sum(mask(:)), ...
        'bounding_box', props.BoundingBox);
end