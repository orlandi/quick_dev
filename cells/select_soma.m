function frame = select_soma(image, varargin)
%SELECT_SOMA process the image so that it is suitable to soma finding.
%   [frame] = select_soma(image, ...)
%   optional arguments:
%   background=3 size of features to remove on background
%   opening=3 size of the opening to isolate the cells
%   edge_thr=0 thresold for finding edges
%   back_thr=0 threshold to removing the background


global CONFIG

dbg=CONFIG.debug;
% for 10x
% background=21; opening=1; edge_thr=1; back_thr=5;

% for 5x
background=background_size(); opening=2; edge_thr=0; back_thr=4;

if ~isempty(varargin)
    background = varargin{1};
    if length(varargin) > 1
        opening = varargin{2};
    end
    if length(varargin) > 2
        edge_thr = varargin{3};
    end
    if length(varargin) > 3
        back_thr = varargin{4};
    end
end

image = 255*rescale_image(image);
if dbg,   figure;imshow(rescale_image(image));title('original');end;

gfilter = fspecial('gaussian', [9 9]);
gbl = imfilter(image, gfilter);

% find the edges
diff = image - gbl;
diff(diff < 0) = 0;
edge_thr = median(diff(:));
edges = floor(diff) > edge_thr;

% edges = imopen(edges, strel('disk', 2, 0));

if dbg,   figure;imshow(edges);title('edges');end;

% remove the background
o_ball = strel('ball', background, background, 0);
e_open = imopen(image, o_ball);
no_back = image - e_open;
% no_back = image - medfilt2(image, [background, background]);
no_back(no_back < 0) = 0;
no_back = rescale_image(no_back);
back_thr = quantile(no_back(:), .75);
if dbg,   figure;imshow(no_back);title('no back');end;
no_back = no_back > back_thr;
if dbg,   figure;imshow(no_back);title('no back (thresholded)');end;

enh = edges & no_back;
if dbg,   figure;imshow(enh);title('enh, pre-opening');end;
enh = imerode(enh, strel('disk', 1, 0));
enh = imdilate(enh, strel('disk', 2, 0));
if dbg,   figure;imshow(enh);title('enh');end;

frame = image .* enh;

%frame = imopen(frame, strel('ball', 1, 1, 0));
% frame = imerode(frame, strel('disk', opening));
% frame = imdilate(frame, strel('disk', opening-1));
% frame = imopen(frame, strel('ball', opening, opening, 0));
if dbg,   figure;imshow(frame);title('frame');end;
end
