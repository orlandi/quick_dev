a = 50*30;
b = 80*30;
d = get_bursts_for_regions(data, [a;b]);
frames = firing_map_for_interval(data, a, b);

for i=1:length(d.burst_time) % all regions
    for j=2:length(d.burst_time{i}) % all bursts
        % cells that fired the frame before
        bt = d.burst_time{i}(j) - a + 1;
        c_b = d.burst_cells{i}{j-1}; % recruited cells before
        c_r = d.burst_cells{i}{j}; % recruited cells
        
        centers_before = reshape([data.cells(c_b).center], 2, size(c_b, 1));
        Xb = centers_before(1,:);
        Yb = centers_before(2,:);
        
        centers = reshape([data.cells(c_r).center], 2, size(c_r, 1));
        X = centers(1,:);
        Y = centers(2,:);
    
        h = figure;
        hold on
        scatter(Xb, Yb, 'ro');%, d.burst_time{i}(j));
        scatter(X, Y, 'b+');%, d.burst_time{i}(j));
        legend(...
            sprintf('before: %d cells', size(Xb, 2)), ...
            sprintf('after: %d cells', size(X, 2)), ...
            'Location', 'SouthOutside' ...
            );
        title(sprintf('region %d - burst %d - activity: %f%%', i, j, 100*d.activity_fraction{i}(bt)));
        
        hold off
        ylim([0 480]);
        xlim([0 640]);
        
        saveas(h, sprintf('%d_%d.png', i, j));
    end % all bursts
end % all regions
%%view(30, 100);
