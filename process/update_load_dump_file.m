function [RUN, data] = update_load_dump_file(dump_file)
S=load(dump_file);

need_update = 0;

% deal with older versions of the dump
if ~isfield(S,'data')
    if ~isfield(S, 'out')
        error('Dump file too old, you have to process the movie again.');
    end
    
    data = S.out;
    data.mean_frame=S.mean_frame;
    data.min_frame=S.min_frame;
    data.max_frame=S.max_frame;
    data.frame=S.frame;
    need_update = 1;
else
    data = S.data;
end

if ~isfield(data, 'cells')
    if isfield(S, 'cells')
        data.cells = S.cells;
        need_update = 1;
    else
        error('could not find cells data!!!');
    end
end

% deal with older versions of the dump
if ~isfield(S, 'RUN')
    RUN=struct();
    RUN.time_run = S.CONFIG.time_run;
    RUN.vidname = S.CONFIG.full_name;
    [RUN.vidpath, RUN.name, ~] = fileparts(RUN.vidname);
    RUN.n_frames = S.CONFIG.n_frames;
    RUN.frame_size = S.CONFIG.frame_size;
    RUN.fps = S.CONFIG.fps;
    need_update = 1;
else
    RUN = S.RUN;
    [RUN.vidpath, RUN.name, ~] = fileparts(RUN.vidname);
end

if need_update
    save(dump_file, 'RUN', 'data', '-v7.3');
end
