function out = process_data(data, work_list)
% out = post_process(data, work_list)
% calls each worklist function with data as argument

out = data;
for work_item = 1:length(work_list)
    out = work_list{work_item}(out);           
end% for

end