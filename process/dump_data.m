function dump_data(dump_file, data)

global RUN

if exist(dump_file, 'file')
    % get the time run
    S = load(dump_file);
    
    % do we have a time_run variable?
    if isfield(S, 'RUN')
        trun = S.RUN.time_run;
    else
        flist = dir(dump_file);
        trun = datenum(flist(1).date);
    end
    
    date_run = datestr(trun, 'yyyymmdd_HHMM');
    copyfile(dump_file,strrep(dump_file, '.mat', ['_' date_run '.mat']));
end

save(dump_file, 'RUN', 'data', '-v7.3');

