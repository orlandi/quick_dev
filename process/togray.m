function [f_bw] = togray(f_color)
    f_bw =   .2989*f_color(:,:,1,:)...
            +.5870*f_color(:,:,2,:)...
            +.1140*f_color(:,:,3,:);
        
        %we passed in specific 4th dim. Can't do 1 and 2 and 3 all at the
        %same time
end