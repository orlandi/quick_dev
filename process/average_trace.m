function out=average_trace(data)
% data = average_trace(data)
% calculates the average trace for each frame, storing it 
% on the average_trace field
out=data;
global RUN
if RUN.from_image
    return
end

out.average_trace = mean(data.trace, 1);    %What is data.trace?
end