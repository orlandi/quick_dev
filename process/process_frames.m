function [ output_data ] = process_frames( data, cells, frame_worklist)
%function [ output_data ] = process_frames( video, frame, cells, frame_worklist )

%PROCESS_FRAMES do the frame-by-frame work
% each function on frame_worklist have to return three items:
% work_function, setup_data, data_name 
% The work per frame will be done like: 
% result{frame} = work_function(setup_data, frame)
% and later stored on a multi-dimensional array at out.(data_name)
% where the last dimension runs from 1:n_frames (number of frames)

%% setup work functions
frame = data.frame;
work_function = {};
setup_data = {};
for i=1:length(frame_worklist)
    [w, s, n] = frame_worklist{i}(frame, cells);
    work_function{i} = w;
    setup_data{i} = s;
    data_names{i} = n;
end

%% load frames and process them
global CONFIG RUN

dbg=CONFIG.debug;
trun = tic;

% let's keep about 4k frames on memory.
% if strfind(computer('arch'), 'win')
%     u = memory;
%     mem = u.MemAvailableAllArrays - u.MemUsedMATLAB;
%     mem = floor(.5*mem);
% 
%     max_frames = mem ./ (4*prod(frame_size));
% 
%     max_frames = 2^(floor(log2(max_frames)));
% end

max_frames = min(CONFIG.max_frames, RUN.n_frames);

if dbg, fprintf('Keeping %d out of %d frames on memory\n', max_frames, RUN.n_frames);end;

frame_data = {};
textprogressbar(sprintf('Processing frames (%d total): ', RUN.n_frames));
ct = tic;
for first_frame=1:max_frames:RUN.n_frames
    last_frame = min(first_frame + max_frames - 1, RUN.n_frames);
    
    % a border case where more than max_frames are missing from the movie
    if first_frame > last_frame
        warning('malformed frame range to load from disk (first=%d, last=%d, n_frames=%d',...
            first_frame, last_frame, RUN.n_frames);
        continue;
    end
    
    asked_frames = last_frame-first_frame+1;
    if dbg, tic;end;

    if(CONFIG.preloadAllFrames)
        frame_buffer = data.orgMovie(:, :, first_frame:last_frame);  
    else
        frame_buffer = zeros([RUN.frame_size length(first_frame:last_frame)]);
        video = open_movie_videoreader(true);
        for i = 1:length(first_frame:last_frame)
            frame_buffer(:, :, i) = getFrame(first_frame+i-1, video);
        end
        close_movie_videoreader(video);
        clear video;
    end
    
    % check if we got all frames, fix the count if negative
    loaded_frames = size(frame_buffer, 3);
    if asked_frames ~= loaded_frames
        fprintf('could not load all frames: asked for %d, got %d.\n',...
            asked_frames, loaded_frames);
        RUN.n_frames = RUN.n_frames - (asked_frames - loaded_frames);
    end
    if dbg, fprintf('loaded %d frames in %5.2f seconds.\n', loaded_frames, toc);end;

    if dbg, tic;end;
    for i=1:size(frame_buffer, 3)
        f = frame_buffer(:,:,i);

        for work_item=1:length(work_function)
            wf = work_function{work_item};
            sd = setup_data{work_item};
            tmp_data{i}{work_item} = wf(f, sd);
        end
        textprogressbar((first_frame+i)/RUN.n_frames*100);
    end
    % copy from temporary to final buffer
    for i=1:size(frame_buffer, 3)
        frame_data{i+first_frame - 1} = tmp_data{i};
    end
    if dbg
        fprintf('done processing of buffer (%d-%d of %d) in %5.2f seconds.\n', ...
        first_frame, last_frame, RUN.n_frames, toc);
    end;
    clear frame_buffer tmp_data
end


if dbg
     save process_frame_dbg.mat -v7.3;
end;
textprogressbar(sprintf('Done! (%.2f s)', toc(ct)));

%% converts to a struct
RUN.n_frames = length(frame_data);

if dbg, tic;fprintf('converting data (%d frames)...', RUN.n_frames);end;
output_data = data;
for data_item=1:length(data_names)
    dn = data_names{data_item};
    
    % figure it out if the data is 1D or more
    data_sample = frame_data{1}{data_item};
    data_size = size(data_sample);
    data_dim = ndims(data_sample);
    if (data_dim == 2) && any(data_size == 1)
        data_size(data_size == 1) = [];
        data_dim = 1;
    end
    
    % transfer the data to an an array
    local_data = zeros([RUN.n_frames data_size], class(data_sample));
    for i=1:RUN.n_frames
        local_data(i,:) = frame_data{i}{data_item};
    end
    
    % and store it on the struct
    output_data.(dn) = local_data';
    
    % clear setup data
    tmp_setup = setup_data{data_item};
    setup_data{data_item}=[];
    clear tmp_setup;
end

% put the cells on data
output_data.cells = cells;

if dbg, fprintf('done in %.2f seconds.\n', toc);end;

clear frame_data

fprintf('processed all frames in %.2f seconds\n', toc(trun));
end % funtion
