function [ frames ] = remove_background(frames)   %***
%REMOVE_BACKGROUND removes background from the frames
%   Removes the background of the supplied frames, using a median filter
%   with size defined by the background size
global CONFIG;
if(CONFIG.useParallelization)
    parforArg = inf;
else
    parforArg = 0;
end

sz = 2*background_size();

parfor (i=1:size(frames, 3), parforArg)
    f = frames(:,:,i);      
    frames(:,:,i) = f - medfilt2(f, [sz sz]);
end


end

