
function [ calc_std ] = frames_std( data, dim )
%FRAMES_STD Calculates standard deviations of data

assert(dim == 3, 'can not really calculate other than 3rd.');

f_size = size(data);
f_size(dim)=[];
calc_std=zeros(f_size);

p = {};

for i=1:f_size(1)
    ps = zeros(1, f_size(2), 'single');             %*** isn't f_size(dim) empty array?
%     a = gpuArray(zeros(size(data, 3), 1));
    di = squeeze(single(data(i,:,:)));              % ***?
    parfor j=1:f_size(2)
%         a=di(j,:);
%         ps(j) = gather(sqrt(sum((a - mean(a)) .^ 2)/length(a)));
        ps(j) = std(di(j,:));
    end;
    p{i} = ps;
end;

for i=1:f_size(1)
    for j=1:f_size(2)
        calc_std(i,j) = p{i}(j);
    end
end

end

