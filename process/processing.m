function data = processing(...
    datafile, frame_worklist, data_worklist, plot_list, export_list)
% data = processing(datafile, frame_worklist, data_worklist)
% process the movie given by datafile, finding cells and then applying
% all items on frame_worklist frame by frame, and then data_worklist on
% the data generated frame-by-frame

%% startup
trun=tic;

%% check the video exists
if ~exist(datafile, 'file')
    error('ioerror:file_not_exists', ...
        'could not find the datafile (%s), aborting.', datafile);
end

global CONFIG
global RUN

%% TODO: change condition, test for .avi extension instead
% if it is not an avi file, we have to search for an dump file
% Lets work with real extensions
[~, ~, ext] = fileparts(datafile);
if(strcmp(ext,'.png'))
    % get the directory
    [fpath, fname, ~] = fileparts(datafile);
    
    % go up one directory and add the dump dir
    base_dir = chop_last_dir(chop_last_dir(fpath));
    fpath = [base_dir 'dumps' filesep];
    
    % get the start of the filename
    fname = regexp(fname, '_raster','split');
    fname = fname{1};
    % get the first listed name
    flist = dir([fpath fname '*.mat']);
    fname = flist(1).name;
    
    % load it
    dump_file = [fpath filesep fname];
    [RUN, data] = update_load_dump_file(dump_file);
    
    RUN.base_dir = base_dir;
end

dbg=CONFIG.debug;

%% store when we started running the script
RUN.time_run = clock;

%% where to dump it

% what to add after the video name
extra_save = '';
if CONFIG.quick
    extra_save = '_quick';
end

if CONFIG.debug
    extra_save = sprintf('_v%s_%dx%s_%s', ...
        CONFIG.version, CONFIG.magnification, ...
        extra_save, ...
        datestr(RUN.time_run, 'yyyy_mm_dd_HHMM')...
        );
end;

% if we are working with a video file
if(~strcmp(ext,'.png'))
    % split the file path
    [RUN.vidpath, RUN.orgName, ~] = fileparts(datafile);
    
    RUN.vidname = datafile;
    RUN.base_dir = RUN.vidpath;
    RUN.videoExtension = ext;
    RUN.from_image = false;
else
    RUN.from_image = true;
end



%%
% open the video and get some information
video = open_movie_videoreader();
%RUN.n_frames = 500; % HACK
% if data is movie, load movie frames.
if(~RUN.from_image && CONFIG.preloadAllFrames)
    orgMovie = load_frames(video);
else
    orgMovie = [];
end
close_movie_videoreader(video);
clear video;
    
% now that movie is loaded, proceed to processing.

RUN.name = strrep([RUN.orgName extra_save], '.', '_');
RUN.figure_path = [RUN.base_dir filesep 'figures' filesep];
RUN.export_path = [RUN.figure_path 'text'];
RUN.dump_path = [RUN.base_dir filesep 'dumps' filesep];
RUN.dump_filename = [RUN.name '.mat'];
RUN.video_path = [RUN.base_dir filesep 'videos' filesep];

if ~exist(RUN.video_path, 'dir'),   mkdir(RUN.video_path); end;
if ~exist(RUN.dump_path, 'dir'),    mkdir(RUN.dump_path); end;
if ~exist(RUN.figure_path, 'dir'),  mkdir(RUN.figure_path); end;
if ~exist(RUN.export_path, 'dir'),  mkdir(RUN.export_path); end;


%%--------------------------

if(~RUN.from_image)
    %% find representative frames
    first = 1;
    
    last = floor(min(RUN.n_frames, CONFIG.minutes_to_sample*60*RUN.fps));

    data.mean_frame = zeros(RUN.frame_size);
    data.min_frame = inf(RUN.frame_size);
    data.max_frame = -inf(RUN.frame_size);
    
    if(~CONFIG.preloadAllFrames)
        video = open_movie_videoreader(true);
    end

    textprogressbar('Processing representative frames: ');

    ct = tic;
    for i = first:last
        if(CONFIG.preloadAllFrames)
            currFrame = orgMovie(:, :, i);  
        else
            currFrame = getFrame(i, video);
        end
        if(~CONFIG.cellDetectionSpeedUp)
            currFrame = remove_background(currFrame);
        end
        currFrame = double(currFrame); % So we can operate with it
        data.mean_frame = data.mean_frame + currFrame;
        data.min_frame = min(data.min_frame, currFrame);
        data.max_frame = max(data.min_frame, currFrame);
        textprogressbar((i-first+1)/length(first:last)*100);
    end
    data.mean_frame = data.mean_frame/length(first:last);
    textprogressbar(sprintf('Done! (%.2f s)', toc(ct)));
    if(CONFIG.cellDetectionSpeedUp)
       disp('Removing background on average frames')
       data.mean_frame = remove_background(data.mean_frame);
       data.min_frame = remove_background(data.min_frame);
       data.max_frame = remove_background(data.max_frame);
       % Needed due to the uint to double conversion
       data.mean_frame(data.mean_frame < 0) = 0;
       data.min_frame(data.min_frame < 0) = 0;
       data.max_frame(data.max_frame < 0) = 0;
    end

    if(~CONFIG.preloadAllFrames)
        close_movie_videoreader(video);
        clear video;
    end
       
    % save them
    figure;imshow(rescale_image(data.mean_frame));savefig(RUN.name, 'mean');close;
    if(CONFIG.useLUTcorrection)
        [minIntensity, maxIntensity] = autoLevelsFIJI(data.mean_frame, RUN.bitDepth);
        RUN.avgFrameMinIntensity = minIntensity;
        RUN.avgFrameMaxIntensity = maxIntensity;
        figure;imshow(data.mean_frame, 'DisplayRange', [minIntensity maxIntensity]);savefig(RUN.name, 'meanCorrected');close;
    end
    figure;imshow(rescale_image(data.min_frame));savefig(RUN.name, 'min');close;
    figure;imshow(rescale_image(data.max_frame));savefig(RUN.name, 'max');close;
    % figure;imshow(rescale_image(std_frame));savefig(name, 'std_dev');close;

    % what we will use for everything else
    data.frame = data.mean_frame;
    data.orgMovie = orgMovie;

    if dbg,   disp('done with representative frames.');end;
    %% debuging?
    save_if_debug;

    %% find cells
    tic;cells = get_cells(data.frame);
    fprintf('found %d cells in %.2f seconds\n', ...
        length(cells), toc);
    figure;show_all_masks(cells, data.frame);savefig(RUN.name, 'cells');close;

    %% debuging?
    save_if_debug;

    %% do the frame-by-frame work
    %         data = process_frames(video, data, cells, frame_worklist);
    data = process_frames(data, cells, frame_worklist);
    %% debuging?
    save_if_debug;
end % if this is an AVI file

%% do the derived data work
data = process_data(data, data_worklist);
clear data.orgMovie     %don't want to save the orginal movie. Too large.
%% debuging?
save_if_debug;

%% export some data
export_data(data, export_list);

%% dump at the end
dump_file = [RUN.dump_path RUN.dump_filename];
dump_data(dump_file, data);

%% make some plots
generate_plots(data, plot_list);


fprintf('completed processing in %7.2f seconds\n', toc(trun));
clear dbg
clear orgMovie
end