###############
version history
###############

0.7.1
=====
* export extra data from bursts
  * 

0.7.0
=====
* calculates and exports burst stats for manually selected regions

0.4.0
=====

* find_regions became a separate operation
* a new algorithm to find regions

0.3.2
=====
* sped up trace extraction (offload to GPU)
* option to run a 'quicker' spike detection, 
  by finding spikes for the first 10s, and then
  selecting the 75% cells that fired the most, and
  extract full firing just for that 75%

0.3.1
=====
* fixed region detection algorithm


version 0.2
-----------
* last data dimension is "time"

version 0.1
-----------
data preparation
* mean/maximum/average frame (without background)
* cell detection

per frame
* trace extraction

data processing
* average trace (for all cells)

plotting
* average trace

---------------
select movie
---------------
* UI for selecting a movie
* (whish) select what work needs to be done
* call processing

----------------------
processing
----------------------
* check movie exists
* find cells
* process what needs to be done for all frames
* process derived data
* dump


================
finding cells
================

* find the background by imopen, using a ball with radius 
  equal to twice the radius of the typical cell
* remove the background of the image
* threshold it (>1)
* find connected components
* loop through connected components
  * if the solidity of the component is smaller than .9
    * find the pixels of the no background image that are
	  part of the "cell"
	* threshold them on their median value
	* find connected components on this image
	* if found more than one component, consider the new components
  * store the area, centroid, mask and bounding box for the component
 
 
==================
per frame processing
==================

============
post-processing
============

############
firing events
############
* break down in different regions based on baseline trace levels
* extact firing events for each region
