revamp data export
==================
* get the file name from the function being run

* make the individual function write to a stream/buffer

* handle the file writing in the job-handling function

ui revamping
============

* (DONE) take care to treat .avi and .png differently

* (DONE) each export creates one file

* (?) change export layout (time on lines, objects on columns)

* (DONE) decouple figure creation/saving from plotting

* (DONE) remembers where we last add files from

* (DONE) do not run find regions/peaks when runing from images

* (DONE) not processing away from the servers

* (DONE) when running movies do not run interactive selections

* (DONE) rename the old dump file when saving

* (DONE) move the dump "normalization" to a separate function

comparative intervals
=====================

* (DONE) ui for selecting regions

  * (DONE) number of regions (not needed, find automatically)

  * (DONE) select two points per region => links two points close as one region
  
  * (DONE) raster plot
    
  * (DONE) select the plot [via code]

* (DONE) calculate intervals for each region

* (DONE) export intervals per region

* (DONE) plot: simple histogram per data file comparing regions

* (DONE) export intervals per cell/region
