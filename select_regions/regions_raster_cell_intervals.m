function data=regions_raster_cell_intervals(data)

if isfield(data, 'regions_raster_selected')
    n_regions=size(data.regions_raster_selected, 2);
    all_intervals = cell(n_regions, 1);

    for i=1:n_regions
        a=data.regions_raster_selected(1, i);
        b=data.regions_raster_selected(2, i);

        all_intervals{i} = intervals_for_cells(data, a, b);
    end

    data.regions_raster_cell_intervals=all_intervals;
end

function intervals_for_cells(data, a, b)
selected =  select_firing_for_interval(data, a, b);

intervals = cell(size(selected));

for i=1:length(selected)
    spks = selected{i};
    if length(spks) > 3
        intervals{i} = spks(2:end) - spks(1:end-1);
    else
        intervals{i} = [];
    end
end

