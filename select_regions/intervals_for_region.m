function intervals=intervals_for_region(data, a, b)
selected =  select_firing_for_interval(data, a, b);

intervals = [];

for i=1:length(selected)
    spks = selected{i};
    if length(spks) > 3
        intervals = [intervals (spks(2:end) - spks(1:end-1))];
    end
end

end