function data=select_regions_raster(data)
% data=SELECT_REGIONS_RASTER(data) is a GUI for selecting 
% analysis regions, based on the raster plot.

global RUN

% only run interactive steps if running from images
if ~RUN.from_image
    return;
end

% displays the gui
[~, pts] = gui_select_regions('UserData',{@plot_raster, data});

% converts to frame count
pts = floor(pts .* RUN.fps);
nframes = size(data.trace, 2);
pts(pts>nframes) = nframes;

data.regions_raster_selected = unfold(pts, 2);
end