function data=regions_raster_intervals(data)

if isfield(data, 'regions_raster_selected')
    n_regions=size(data.regions_raster_selected, 2);
    all_intervals = cell(n_regions, 1);

    for i=1:n_regions
        a=data.regions_raster_selected(1, i);
        b=data.regions_raster_selected(2, i);

        all_intervals{i} = intervals_for_region(data, a, b);
    end

    data.regions_raster_intervals=all_intervals;
end    