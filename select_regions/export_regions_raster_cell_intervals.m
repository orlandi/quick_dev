function export_regions_raster_cell_intervals( fout, data )
%EXPORT_INTERVALS_FOR_REGION intervals for the manually selected regions

% TESTED WITH:
% data=struct();
% data.regions_raster_cell_intervals=cell(2,1);
% data.regions_raster_selected=[1,20;35,14];
% data.regions_raster_cell_intervals{1}{1}=[1,2,3];
% data.regions_raster_cell_intervals{1}{2}=[3,2];
% data.regions_raster_cell_intervals{1}{3}=[1,2,3,5,6,7,8,10];
% data.regions_raster_cell_intervals{2}{1}=[1,3];
% data.regions_raster_cell_intervals{2}{2}=[3,2,5,4,3,1,2,3,1,54,5,1];
% data.regions_raster_cell_intervals{2}{3}=[1,2,3,5,6];
% fout=fopen('export_test.txt','w');export_regions_raster_cell_intervals(fout, data);fclose(fout);

% header
fprintf(fout, '##\n##cell intervals for region\n##\n');
fprintf(fout,'#intervals_cell_1\tintervals_cell_2\t...\tintervals_cell_n\n');

n_regions = length(data.regions_raster_cell_intervals);
n_cells = length(data.regions_raster_cell_intervals{1});

for i=1:n_regions
    r_count = cellfun(@length, data.regions_raster_cell_intervals{i});
    longest = max(r_count);
    
    a=data.regions_raster_selected(1, i);b=data.regions_raster_selected(2, i);
    fprintf(fout, '#region %d - (%d,%d)\n', i, a, b);
    for t=1:longest
        for j=1:n_cells
            if j>1
                fprintf(fout, '\t');
            end
            if t <= r_count(j)
                fprintf(fout, '%d',data.regions_raster_cell_intervals{i}{j}(t));
            end
        end
        fprintf(fout, '\n');
    end
end
end