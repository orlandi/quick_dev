function data=regions_raster_trains(data)

global RUN

%do not run interactive features when running from movies
if ~RUN.from_image
    return;
end

n_regions=size(data.regions_raster_selected, 2);
all_trains = cell(n_regions, 1);

for i=1:n_regions
    a=data.regions_raster_selected(1, i);
    b=data.regions_raster_selected(2, i);
    
    all_trains{i} = select_firing_for_interval(data, a, b, 1);
end

data.regions_raster_trains=all_trains;
    