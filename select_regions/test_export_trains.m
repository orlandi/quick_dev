% TESTED WITH:
global RUN
if exist('RUN', 'var')
    old_run = RUN;
    clear RUN;
end

if exist('data', 'var')
    old_data = data;
    clear data;
end

RUN = struct();
RUN.from_image = 1;

data=struct();
% 2 cells
data.cells = [ 1 2];

% 4 regions
data.regions_raster_selected = [ 0 7 15 20;4 10 18 72];
data.regions_raster_trains=cell(3,1);
reg = cell(2, 1);
reg{1} = [1, 3]; reg{2} = [];
data.regions_raster_trains{1} = reg;

reg = cell(2, 1);
reg{1} = []; reg{2} = [1, 6];
data.regions_raster_trains{2} = reg;

reg = cell(2, 1);
reg{1} = [1 5]; reg{2} = [4 7];
data.regions_raster_trains{3} = reg;

reg = cell(2, 1);
reg{1} = []; reg{2} = [];
data.regions_raster_trains{4} = reg;


fout=fopen('/Users/girotto/data/tests/export_test.txt','w');
export_regions_raster_trains(fout, data);
fclose(fout);

if exist('old_run', 'var')
    RUN = old_run;
    clear old_run;
end

if exist('old_data', 'var')
    data = old_data;
    clear old_data;
end