function export_regions_raster_intervals( fout, data )
%EXPORT_INTERVALS_FOR_REGION intervals for the manually selected regions

% TESTED WITH:
% data=struct();
% data.regions_raster_intervals=cell(2,1);
% data.regions_raster_intervals{1}=[1,2,3];
% data.regions_raster_intervals{2}=[1,2,3,4,5,6];
% fout=fopen('/Users/girotto/data/tests/export_test.txt','w');
% export_regions_raster_intervals(fout, data);
% fclose(fout);

global RUN

if ~RUN.from_image
    return;
end

% header
fprintf(fout, '##\n##intervals for region\n##\n');

fprintf(fout,'#intervals_region_1\tintervals_region_2\t...\tintervals_region_n\n');

n_regions = length(data.regions_raster_intervals);
r_count = cellfun(@length, data.regions_raster_intervals);
longest = max(r_count);

for t=1:longest
    for i=1:n_regions
        if i>1
            fprintf(fout, '\t');
        end
        if t <= r_count(i)
            fprintf(fout, '%d',data.regions_raster_intervals{i}(t));
        end
    end
    fprintf(fout, '\n');
end
end
