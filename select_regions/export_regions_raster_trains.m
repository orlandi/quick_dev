function export_regions_raster_trains( fout, data )
%EXPORT_REGIONS_RASTER_TRAINS spike trains for selected regions.
global RUN

if ~RUN.from_image
    return;
end

% header
fprintf(fout, '##\n##trains for region\n##\n');
fprintf(fout, '##num_events;ev1,ev2,ev3,....\n');
fprintf(fout, '##firing_events\n');

n_regions = size(data.regions_raster_selected, 2);

for i=1:n_regions
    a=data.regions_raster_selected(1, i);
    b=data.regions_raster_selected(2, i);

    fprintf(fout,'##region %d: [%d,%d)\n', i, a, b);

    ncells = length(data.cells);
    for t_i=1:ncells
        train = data.regions_raster_trains{i}{t_i};
        fprintf(fout, '%d;',length(train));
        for spk=1:length(train)
            if spk>1
                fprintf(fout, ',');
            end
            fprintf(fout, '%d', train(spk));
        end
        fprintf(fout, '\n');
    end
end

end
