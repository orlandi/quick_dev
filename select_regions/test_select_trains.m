% TESTED WITH:
if exist('RUN', 'var')
    old_run = RUN;
    clear RUN;
else
    global RUN
end

if exist('data', 'var')
    old_data = data;
    clear data;
end

RUN = struct();
RUN.from_image = 1;

data=struct();
% 2 cells
data.cells = [ 1 2];
data.peak_locations = cell(2,1);
data.peak_locations{1} = [1 10 11 13 15 27];
data.peak_locations{2} = [2 3 4 15 20];

% 4 regions
data.regions_raster_selected = [ 1 7 15 20;4 10 18 72];

data = regions_raster_trains(data);

assert(isfield(data, 'regions_raster_trains'), 'field does not exist');
assert(length(data.regions_raster_trains) == 4, 'wrong number of regions');

% region 1
pks = data.regions_raster_trains{1};
l=length(pks{1});ex=1;assert(l == ex, 'expected %d, got %d', ex, l);
l=length(pks{2});ex=2;assert(l == ex, 'expected %d, got %d', ex, l);
l=pks{1}(1);ex=0;assert(l == ex, 'expected %d, got %d', ex, l);
l=pks{2}(1);ex=1;assert(l == ex, 'expected %d, got %d', ex, l);

% region 2
pks = data.regions_raster_trains{2};
l=length(pks{1});ex=0;assert(l == ex, 'expected %d, got %d', ex, l);
l=length(pks{2});ex=0;assert(l == ex, 'expected %d, got %d', ex, l);

% region 3
pks = data.regions_raster_trains{3};
l=length(pks{1});ex=1;assert(l == ex, 'expected %d, got %d', ex, l);
l=length(pks{2});ex=1;assert(l == ex, 'expected %d, got %d', ex, l);
l=pks{1}(1);ex=0;assert(l == ex, 'expected %d, got %d', ex, l);
l=pks{2}(1);ex=0;assert(l == ex, 'expected %d, got %d', ex, l);

% region 4
pks = data.regions_raster_trains{4};
l=length(pks{1});ex=1;assert(l == ex, 'expected %d, got %d', ex, l);
l=length(pks{2});ex=1;assert(l == ex, 'expected %d, got %d', ex, l);
l=pks{1}(1);ex=7;assert(l == ex, 'expected %d, got %d', ex, l);
l=pks{2}(1);ex=0;assert(l == ex, 'expected %d, got %d', ex, l);

clear l ex pks

if exist('old_run', 'var')
    RUN = old_run;
    clear old_run;
end

if exist('old_data', 'var')
    data = old_data;
    clear old_data;
end