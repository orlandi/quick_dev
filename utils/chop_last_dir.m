function chopped = chop_last_dir(dir)
% chop_last_dir removes the last part of a directory.

    d = dir;

    if ~(max(strfind(d, filesep))==length(d))
        d = [d filesep];
    end
    slashes = strfind(d, filesep);
    lst = slashes(end - 1);
    chopped = d(1:lst);
end