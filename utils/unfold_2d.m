function [ out ] = unfold_2d( arr, tau )
%unfold_2d unfolds the 2nd dimension of a 2d array into a 3rd dimension
% Given a 2d array (arr):
% a11 a12 a13 ... a1n
% a21 a22 a23 ... a2n
% ...
% am1 am2 am3 ... amn
% 
% unfold_2d(arr, k) will return a 3d array:
% a(:,:,1) = ...
%   a11 a1k a1(2k) ... a1(lk)
%   a21 a2k a2(2k) ... a2(lk)
%   ...
% a(:,:,2) = ...
%   a12 a1(k+1) a1(2k+1) ... a1(lk+1)
%   ...
% and so forth

n_size = floor([size(arr) tau] ./ [1 tau 1]);
out = zeros(n_size, class(arr));

idx = (1:n_size(2)) - 1;
for i=1:tau
    out(:,:,i) = arr(:,i + idx*tau);
end
end

