function selected = select_firing_for_interval(data, a, b, reset_train)
% selected = selected_firing_for_interval(data, a, b)
% selects all firing events that happened between frames a and b, returning
% the individual firing times for each cell.

if nargin < 4
    reset_train = 0;
end

selected = cell(length(data.cells), 1);
for i = 1:length(selected)
    p = data.peak_locations{i};
    
    % select the firing events between stimulation
    p = p( (p>=a) & (p<b) );
    if reset_train && ~isempty(p)
        p = p - a + 1;
    end
    selected{i} = p;
end
end