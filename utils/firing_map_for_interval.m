function [ frames ] = firing_map_for_interval( data , a, b )
%FIRING_MAP_FROM_EVENTS Summary of this function goes here
%   Detailed explanation goes here

    region_events = select_firing_for_interval(data, a, b, 1);
    frames = false(length(region_events), b-a+1);
    for c = 1:length(region_events)
        frames(c, region_events{c}) = 1;
    end % for cells
end

