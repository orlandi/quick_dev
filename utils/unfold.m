function [ret] = unfold(ts, dim)
ret = zeros(dim, length(ts) - dim);
for i=1:(length(ts) - dim + 1)
    ret(:,i) = ts(i + (0:(dim-1)))';
end
end