function [medians] = time_medians(ts, window_size)
medians = median(unfold(ts, window_size));
end