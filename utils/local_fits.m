function slopes = local_fits(what, trend_length)
global CONFIG;
if(CONFIG.useParallelization)
    parforArg = inf;
else
    parforArg = 0;
end

mm = unfold(what, trend_length);
xx = [ones(trend_length,1), (1:trend_length)'];
xx_op = (xx'* xx)\(xx');
slopes = zeros(1,length(what));
parfor (i=1:length(what)-trend_length, parforArg)
    yy = mm(:,i);
    beta = xx_op * yy;
    slopes(i) = beta(2);
end
slopes = filter(ones(1,trend_length)/trend_length,1,slopes);
slopes(1:60)=0;
end
