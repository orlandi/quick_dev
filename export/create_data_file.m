function f_out=create_data_file(fname)

% deal with the file already existing
if exist(fname, 'file')
    f = fopen(fname, 'r');
        % data about when the file was run is at the first line
        date_run = sscanf(fgetl(f), '#time run:%s');
    fclose(f);

    copyfile(fname,strrep(fname, '.txt', ['_' date_run '.txt']));
    delete(fname);
end

f_out = fopen(fname, 'w');

end