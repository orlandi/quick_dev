function write_general_header(f_out)
global CONFIG RUN

% first thing to write: time the script was run
fprintf(f_out, '#time run:%s\n', datestr(RUN.time_run, 'yyyymmdd_HHMM'));

%% write some extra general information

% code version
fprintf(f_out, '#code version:%s\n', CONFIG.version);
% magnification
fprintf(f_out, '#magnification:%d\n', CONFIG.magnification);
% full file name
fprintf(f_out, '#video source:%s\n', RUN.vidname);


end