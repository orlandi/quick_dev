function export_data( data, worklist )
%export_data exports data to a text file at the same path as the movie.
%  The file contains a header with general information followed by sections
%  defined by the list of functions worklist
global RUN

% section specific export
for i=1:length(worklist)
    % each function on worklist should write a small header describing its
    % section, so that users know what it is about
    
    ex_fun = worklist{i};
    
    % figure out the name of the file to export
    fname = [...
        RUN.export_path filesep ...
        RUN.name strrep(func2str(ex_fun), 'export', '') ...
        '.txt' ...
        ];
    
    % creates a file to export the data
    f_out = create_data_file(fname);
    write_general_header(f_out);
    ex_fun(f_out, data);
    
    % close the file and we're done
    fclose(f_out);
end

end