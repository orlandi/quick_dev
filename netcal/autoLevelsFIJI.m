function [minIntensity, maxIntensity] = autoLevelsFIJI(imData, bpp)
    pixelCount = numel(imData);
    limit = pixelCount/10;
    threshold = pixelCount/5000;
    [histCount, histValues] = hist(imData(:), 0:2^bpp-1);
    minIntensity = 0;
    for i = 1:length(histValues)
        count = histCount(i);
        if(count > limit)
            count = 0;
        end
        if(count > threshold)
            minIntensity = i-1;
            break;
        end
    end
    maxIntensity = 2^bpp-1;
    for i = length(histValues):-1:1
        count = histCount(i);
        if(count > limit)
            count = 0;
        end
        if(count > threshold)
            maxIntensity = i-1;
            break;
        end
    end
end