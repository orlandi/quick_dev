function img = getFrame(frame, fid)
% GETFRAME get a given frame from a preloaded movie
%
% USAGE:
%    img = getFrame(frame, fid)
%
%    frame - selected frame (starts at 1)
%
% OUTPUT arguments:
%    img - frame data

global CONFIG RUN
    
% Something else in case it is a .dcimg file
if(strcmpi(RUN.videoExtension,'.dcimg'))
    fseek(fid, 232 + RUN.experiment.frameSize*(frame-1), 'bof');
    img = fread(fid, RUN.experiment.frameSize/(RUN.experiment.bpp/8), RUN.experiment.pixelType);
    %img = img(1:experiment.width*experiment.height); % In case it is not the full image
    %img = fread(fid,frameSize/2, 'int16');
    %img = reshape(img, [experiment.width, experiment.height])';
    img = reshape(img, [RUN.experiment.frameSize/RUN.experiment.height/(RUN.experiment.bpp/8), RUN.experiment.height])';
    img = img(:, 1:RUN.experiment.width);
% If it's an AVI
elseif(strcmpi(RUN.videoExtension, '.avi'))
    img = read(fid, frame);
    % Turn to grayscale
    if(size(img, 3) == 3)
        img = rgb2gray(img);
    end
end

if ~isempty(CONFIG.frame_filter)
    img = imfilter(img, CONFIG.frame_filter);
end

