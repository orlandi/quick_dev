function [ frames ] = load_frames( video )
%frames = LOAD_FRAMES(video, first, last) 
%Loads frames from video, starting by first 
%and goind until last, converting them to 
%grayscale.

%Edit: load frames loads ALL frames from the movie without adjustment. 
% There is an adjustment (frame_filter)

    global CONFIG RUN
    
    frames = zeros([RUN.frame_size, RUN.n_frames], RUN.pixelType);
    ct = tic;
    textprogressbar('Loading all frames: ');
    for i = 1:RUN.n_frames
        frames(:, :, i) = getFrame(i, video);    
        if ~isempty(CONFIG.frame_filter)
            frames(:, :, i) = imfilter(frames(:, :, i), CONFIG.frame_filter);
        end
        textprogressbar(i/RUN.n_frames*100);
    end
    textprogressbar(sprintf('Done! (%.2f s)', toc(ct)));
end