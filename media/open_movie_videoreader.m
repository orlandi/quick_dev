function [video] = open_movie_videoreader(varargin)
global RUN
if(nargin == 1)
    alreadyDone = varargin{1};
else
    alreadyDone = false;
end

if(strcmpi(RUN.videoExtension,'.avi'))
    video = VideoReader(RUN.vidname);
    if(alreadyDone)
        return;
    end

    RUN.n_frames = floor(video.NumberOfFrames);
    RUN.frame_size = floor([video.Height, video.Width]); %frame size
    RUN.fps = video.FrameRate;
    bitDepth = video.BitsPerPixel;
elseif(strcmpi(RUN.videoExtension,'.dcimg'))
    
    video = fopen(RUN.vidname, 'r');
    if(alreadyDone)
        return;
    end
    experiment.handle = RUN.vidname;
    fseek(video, 0, 'bof');
    hdr_bytes = fread(video, 232);

    bytes_to_skip = 4*fromBytes(hdr_bytes(9:12));
    curr_index = 8 + 1 + bytes_to_skip;

    % Number of frames
    nFrames = fromBytes(hdr_bytes(curr_index:(curr_index+3)));

    curr_index = 48 + 1;
    fileSize = fromBytes(hdr_bytes(curr_index:curr_index+7));

    % bytes per pixel
    curr_index = 156 + 1;
    bitDepth = 8*fromBytes(hdr_bytes(curr_index:curr_index+3));

    % footer location 
    %curr_index = 120 + 1;
    %footerLoc = fromBytes(hdr_bytes(curr_index:curr_index+7));
    % funny entry pair which references footer location - This one
    curr_index = 192 + 1;
    odd = fromBytes(hdr_bytes(curr_index:curr_index+7));

    curr_index = 40 + 1;
    offset = fromBytes(hdr_bytes(curr_index:curr_index+7));
    footerLoc = odd+offset;


    % number of columns (x-size)
    curr_index = 164 + 1;
    xsize_req = fromBytes(hdr_bytes(curr_index:curr_index+3));

    % bytes per row
    curr_index = 168 + 1;
    bytes_per_row = fromBytes(hdr_bytes(curr_index:curr_index+3));
    %if we requested an image of nx by ny pixels, then DCIMG files
    %for the ORCA flash 4.0 still save the full array in x.
    xsize = bytes_per_row/2;

    % binning
    % this only works because MOSCAM always reads out 2048 pixels per row
    % at least when connected via cameralink. This would fail on USB3 connection
    % and probably for other cameras.
    % TODO: find another way to work out binning
    binning = (4096/bytes_per_row);


    % number of rows
    curr_index = 172 + 1;
    ysize = fromBytes(hdr_bytes(curr_index:curr_index+3));

    % TODO: what about ystart? 

    % bytes per image
    curr_index = 176 + 1;
    frameSize = fromBytes(hdr_bytes(curr_index:curr_index+3));

    % Now the weird part to get the frame rate
    fseek(video, footerLoc+272+nFrames*4, 'bof');
    val = zeros(nFrames, 1);
    % Get all time stamps
    for i = 1:nFrames
        a = fread(video, 4);
        b = fread(video, 4);
        val(i) = decodeFloat(a, b);
    end

    % Much easier
    fps = 1/((val(end)-val(1))/nFrames);

    if(bitDepth == 16)
        pixelType = '*uint16';
    elseif(bitDepth == 8)
        pixelType = '*uint8';
    elseif(bitDepth == 32)
        pixelType = '*uint32';
    end

    experiment.metadata = 'no metadata for .dcimg';
    experiment.numFrames = nFrames;
    experiment.fps = fps;
    experiment.totalTime = experiment.numFrames/experiment.fps;
    experiment.width = xsize_req;
    experiment.height = ysize;
    experiment.pixelType = pixelType;
    experiment.bpp = bitDepth;
    experiment.frameSize = frameSize;
    experiment.binning = binning;
    
    RUN.n_frames = experiment.numFrames;
    RUN.frame_size = [experiment.width experiment.height];
    RUN.fps = experiment.fps;
    RUN.experiment = experiment;
    
end

if(bitDepth == 16)
    RUN.pixelType = 'uint16';
elseif(bitDepth == 8)
    RUN.pixelType = 'uint8';
elseif(bitDepth == 32)
    RUN.pixelType = 'uint32';
end
RUN.bitDepth = bitDepth;

end
