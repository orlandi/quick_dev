function plot_post_stimulation_stats(data)

neurons = data.num_peaks > 0;
X = find(neurons);

figure;
  scatter(X, data.post_paired_cell_mean_count(neurons));

figure;
  errorbar(...
      X, ...
      data.post_paired_cell_mean_interval(neurons), ...
      data.post_paired_cell_std_interval(neurons), ...
      'bd');

c = data.post_paired_cell_counts;
i = data.post_paired_cell_interval;

figure;
  hist(c(c>0));
  title('Histogram of post-stimulation firing count');
  
figure;
  hist(i(c>2));
  title('Histogram of post-stimulation mean interval');
  

end
