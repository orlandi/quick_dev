function [data] = post_stimulation_stats(data, fps)
% For each stimulation interval, and each cell calculates some statistics
% * number of times fired
% * mean and standard deviation of the time between ,
%   if it fired more than 3 times

if nargin < 2; fps=30;end;

[inner, outer, first] = stimulation_intervals(data.activity_fraction);

K = 1;
next = first;
counts = [];
means = [];
stds = [];

stimulated_counts = [];
stimulated_means = [];

intervals = [];

intervals_stimulated = [];

end_time = size(data.trace, 2);
% we will stop when there is no more movie
while (next+inner)<end_time
    [selected, fired, next] = select_firing_for_interval(K,data);

    for i=1:length(selected)
        firing = selected{i};
        
        if length(firing)>3
            this_intervals = (firing(2:end)-firing(1:end-1));
            
            intervals = [intervals this_intervals];
            
            if fired(i)
                intervals_stimulated = [intervals_stimulated  this_intervals];
            end
        end
    end
    this_counts = cellfun(@length, selected)';
    this_means = cellfun(@f_int_mean, selected)' / fps;
    
    stimulated_counts = [stimulated_counts this_counts(fired)];
    stimulated_means = [stimulated_means this_means(fired)];
    
    counts = [counts; this_counts];
    means  = [means; this_means];
    stds   = [stds; cellfun(@f_int_std, selected)' / fps];
    
    K = K+1;
end

mean_count    = zeros(1, size(counts, 2));
mean_interval = zeros(1, size(counts, 2));
std_interval  = zeros(1, size(counts, 2));

for i=1:size(counts, 2)
    c = counts(:, i);
    m = means(:, i);
    
    mean_count(i)    = mean(c(c>0));
    mean_interval(i) = mean(m(m>0));
    std_interval(i)  = std(m(m>0));
end;



data.post_paired_cell_counts = counts;
data.post_paired_cell_interval = means;
data.post_paired_cell_interval_std = stds;

data.post_paired_stimulated_counts = stimulated_counts;
data.post_paired_stimulated_means = stimulated_means;

data.post_paired_cell_mean_count = mean_count;
data.post_paired_cell_mean_interval = mean_interval;
data.post_paired_cell_std_interval = std_interval;

data.post_paired_all_intervals = intervals;
data.post_paired_all_intervals_stimulated = intervals_stimulated;
end

function [m]=f_int_mean(s)
m = 0;
if length(s)>2
    m = mean(s(2:end) - s(1:end-1));
end
end

function [m] = f_int_std(s)
m = 0;
if length(s)>3
    m = std(s(2:end)-s(1:end-1));
end
end
