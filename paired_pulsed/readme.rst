how it works
============

stimulation_intervals
---------------------

* find peaks on the activity fraction, assuming (as default) a minimum
  activity of 30% to be considered a peak

* calculate the unique intervals between peaks

* the internal paired pulse interval is the minimum of those unique intervals

* the time between successive paired pulse applications it the maximum of
  those intervals plus the internal interval

For example, for one movie the unique intervals were found to be 57, 542, 543.

* internal interval = 57, external interval = 600, 1.9s between pulses, 20s
  between two consecutive paired pulse starts.


select_firing_for_interval
--------------------------

Select only the firing times of events that happened between 
the Kth and (K+1)th paired pulsed stimulations

post_stimulation_stats
----------------------

For each stimulation interval, and each cell calculates some statistics

* number of times fired

* mean and standard deviation of the time between ,
  if it fired more than 3 times


