function [inner,outer, first] = stimulation_intervals(activity_fraction, threshold)
% [outer, inner] = stimulation_intervals(activity_fraction, threshold)
% Finds the time between bursts, and the inter-burst time

if nargin < 2;threshold = 0.3;end

[~, locs] = findpeaks(activity_fraction, 'MINPEAKHEIGH', threshold);

intervals = unique(locs(2:end)-locs(1:end-1));
inner = min(intervals);
outer = max(intervals)+inner;

first = locs(1);
end