function plot_detected_paired_stimulation(data)

global RUN

A = data.activity_fraction;
[inner, outer] = stimulation_intervals(A);

[~, locs] = findpeaks(A, 'MINPEAKHEIGH',0.25);

X = 1:length(A);
X = X/30;

stairs(X, A);
hold on;
plot(X(locs), A(locs), 'ro');
hold off;
title(...
  sprintf('Movie: %s.\ninner: %7.5f, outer: %7.5f\n',...
        strrep(RUN.name, '_', '-'),...
        inner/30, ...
        outer/30));

end