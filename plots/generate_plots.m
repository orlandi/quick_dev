function generate_plots(data, plot_list)
% generate_plots(data, plot_list)
% generates a list of plots, saving and closing
global RUN

for work_item=1:length(plot_list)
    plot_fn = plot_list{work_item};
    % create a figure and plot
    f=figure;plot_fn(data);
    % save the figure
    savefig(RUN.name, strrep(func2str(plot_fn), 'plot_', ''));
    % close the figure
    close(f);
end
end
