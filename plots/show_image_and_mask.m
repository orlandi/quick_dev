function show_image_and_mask( Image, Mask )
%SHOWIMAGEANDMASK Summary of this function goes here
%   Detailed explanation goes here
    
img = rescale_image(Image);
mask = single(Mask);
invmask = single(not(Mask));
PlotImage = cat(3, ...
        mask .* ones(size(img)), ...
        invmask .* img, ...
        invmask .* img);
imshow(PlotImage, 'InitialMagnification', 'fit');
imshow(PlotImage);
end

