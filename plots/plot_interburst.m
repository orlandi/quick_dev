function plot_interburst( data )
global RUN
colors = {'b','r','g'};
lines = { '-','--','-.',':'};
style = 0;

legends = {};

hold on;

for i=1:length(data.region_interburst_time)
    if length(data.region_interburst_time{i}) < 3
        % not enough stats
        continue
    end
    style = style + 1;
    st_str = [colors{mod(style, 3)+1} lines{mod(style, 4)+1}];

    [f x] = ecdf(data.region_interburst_time{i});

    stairs(x, f, st_str);
    legends = [legends sprintf('region %d; %6.3f(%6.3f)s => %6.3f Hz', ...
        i, ...
        data.region_mean_interburst(i),std(data.region_interburst_time{i}),...
        1 ./ data.region_mean_interburst(i))];
end
for i=1:length(legends)
    if isempty(legends{i})
        legends{i} = [];
    end
end

xlabel('time between bursts (s)');set(gca,'XMinorTick', 'on');
ylabel('Cumulative probability');
legend(legends{:}, 'Location', 'SouthEast');

title(strrep(RUN.name, '_', ' '));
hold('off');
end

