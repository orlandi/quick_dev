function plot_raster(data)
global CONFIG RUN

j = 1;

X = {};
yyy = ones(size(data.trace, 2), 1);
for i=1:size(data.trace, 1)
   if data.num_peaks(i) <= CONFIG.min_firing_for_active
        continue;
    end
    pks = data.peak_locations{i}/30;
    if length(pks)<3
        continue
    end
    X{3*j-2} = pks;X{3*j-1}=yyy(1:length(pks))*j;X{3*j}='k.';
    j = j+1;
end;

plot(X{:},'HitTest', 'off');
xlabel('time (s)');

title(strrep(RUN.name, '_', ' '));
end
