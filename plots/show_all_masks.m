function show_all_masks(cells, img)
allmasks = false(size(img));
for i=1:length(cells)
    %allmasks = or(allmasks, cells(i).mask);
    allmasks(cells(i).mask) = true;
end
show_image_and_mask(img, allmasks);

% label the cells
colors=['b' 'g' 'c' 'm' 'y'];
for i=1:length(cells)
    row = cells(i).center(1);
    col = cells(i).center(2);
    cidx = mod(i,length(colors))+1;
    h = text(col+1, row-1, num2str(i));
    set(h,'Color',colors(cidx), 'FontSize',7);
end;
end
