function plot_activity_fraction( data )

global RUN

hold on;
x = (1:RUN.n_frames) ./ (30*60); % minutes
plot(x, data.activity_fraction, 'b.');
plot([0,x(end)], [0.1, 0.1], 'r');
xlabel('time (minutes)');
ylabel('fraction of cells active');

title(strrep(RUN.name, '_', ' '));
hold('off');
axis('tight');

end