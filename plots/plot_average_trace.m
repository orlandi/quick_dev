function plot_average_trace(data)
global RUN
x = 1:length(data.average_trace);
x = x/30/60;                            %*** ?

plot(x, data.average_trace);
xlabel('time (minutes)');
ylabel('average trace value');
title(strrep(RUN.name, '_', '-'));

end