function [ out ] = later_interval_hist( data )
%LATER_INTERVAL_HIST Calculates a firing interval histogram after stimulation
%   Calculates a firing interval histogram after stimulation.
%   The underlying assumption is that half the recording duration is
%   pre-stimulation, and 

% find regions that begin after half
regions = usable_regions(data);

% for each region
%   calculate intervals
intervals = [];
for i=1:size(regions,2)
    a=regions(1,i);b=regions(2,i);
    
    intervals = [intervals; intervals_for_region(data, a, b)];
end

% create bins (1, 2, 3, inf) and calculate histograms
b_edges = [1 2 3 4 inf];
N_int = histc(intervals, b_edges);
N = sum(N_int);
n_int = N_int(1:3) ./ [1;1;1] / N;

out = data;
out.later_intervals     = intervals;
out.later_interval_hist = n_int;
end

function regions = usable_regions(data, start, finish)
regions = [];
for i=1:size(data.regions, 2)
    a=data.regions(1, i);
    b=min(data.regions(2,i), finish);
    if b>=a
        continue
    end
    
    if a < start && b <= finish
        regions = cat(2, regions, [start;b]);
    elseif a >= start
        regions = cat(2, regions, [a;b]);
    end
end
end

function intervals=intervals_for_region(data, a, b)
    fired = select_firing_for_interval(data, a, b);
    intervals = [];
    for i=1:length(fired)
        t = fired{i};
        if length(t)>2
            intervals = [intervals;(t(2:end)-t(1:end-1))'];
        end
    end
end