function out=process_frame(frame, varargin)
out = cell(nargin, 1);
for i=1:length(varargin)
    out{i} = varargin{i}(frame);
end
end