start_idx=1;end_idx=8*15;delta=8*5;
complete = [];
cc = {};
for ext=start_idx:delta:end_idx
    data = randn(640, 480, delta);
    temp = {};
    parfor i=ext:(ext+delta-1)
        f = data(:,:,i-ext+1);
        in  = {@mean, @var, @kurtosis};
        out = process_frame(f(:), in{:});
        cc{i} = out(:);
    end
end
disp(size(cc));
disp(size(cc{1}));
    