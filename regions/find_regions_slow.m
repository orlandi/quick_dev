function [ data ] = find_regions_slow( in_data )
%FIND_REGIONS find regions with different characteristics from traces

data=in_data;
global RUN

if RUN.from_image
    return
end


trun = tic;             % *** whats tic and trun?
traces = data.trace;

% low-pass the median of all traces
% and get the fast and slow local slopes
filtered=filter(ones(1,30)/30,1, median(traces,1));     % *** 
slow_slopes = local_fits(filtered, 300);

% find transitions
last = 1;
inside = false;
outside = true;
thr_rise = 0.01; thr_dec = 0.0075;

abs_slopes = abs(slow_slopes);
rise = abs_slopes > thr_rise;
decay = abs_slopes < thr_dec;
trans = [];
i = 0;
dx = 30;
while i < length(slow_slopes)
    i = i + 1;

    continuation = ((i-last)>dx) && (slow_slopes(i)*slow_slopes(last)>0);
    if (~inside) && outside && rise(i)
        inside = true;
        outside = false;
        trans = [trans i];
    elseif inside && decay(i)
        inside = false;
        last = i;
    elseif ~inside && ~outside && ~continuation && ~decay(i)
        last = i;
    elseif ~inside && ~outside && decay(i) && continuation
        trans = [trans i];
        outside = true;
    end
end

figure;
    subplot(211);plot(filtered);title('filtered median trace');axis tight;
    subplot(212);
        hold on;
        plot(slow_slopes);
        plot(trans, slow_slopes(trans), 'ro');
        title('slow slope');axis tight;
        hold off;
    savefig(RUN.name, 'find_regions_slow');
close;

% shuffle around borders
dx = 30;
while true
    sel = [trans(1:end-1)+dx >= trans(2:end) false];
    if ~any(sel)
        break
    end
    trans(sel) = max(1, trans(sel)-dx);
end

shuffled = [1];
l = size(traces,2);
for i=1:length(trans)
    b = min(trans(i)-dx, l);
    t = min(trans(i)+dx, l);
    if (b==l || t==l) || (b<=shuffled(end))
        continue
    end
    shuffled = [shuffled, b, t];
end
trans = [shuffled size(traces,2)];

regions = unfold(trans, 2);

for i=1:size(regions,2)
    if i>1
        regions(1, i) = regions(1, i)+1; % so that regions do not overlap!
    end

    fprintf('Region %d, [%d,%d] is %7.2f s long.\n',...
        i, ...
        regions(1,i),regions(2,i),...
        (regions(2,i)-regions(1,i))/30);
end

data.regions = regions;
fprintf('Found regions in %5.3fs.\n',toc(trun));
end

