function [ data ] = find_regions( in_data )
%FIND_REGIONS find regions with different characteristics from traces

data=in_data;

global CONFIG RUN
if RUN.from_image
    return
end
if(CONFIG.useParallelization)
    parforArg = inf;
else
    parforArg = 0;
end

trun = tic;
traces = data.trace;

% low-pass the median of all traces
% and get the fast and slow local slopes
filtered=filter(ones(1,30)/30,1, median(traces,1));
fast_slopes = local_fits(filtered, 5, parforArg);
slow_slopes = local_fits(filtered, 300, parforArg);

figure;
    subplot(311);plot(filtered);title('filtered median trace');axis tight;
    subplot(312);plot(fast_slopes);title('fast slope');axis tight;
    subplot(313);plot(slow_slopes);title('slow slope');axis tight;
    savefig(RUN.name, 'find_stimulus');
close;

% find transitions
warning('off','signal:findpeaks:largeMinPeakHeight');
    thr_p = fast_and_slow(fast_slopes, slow_slopes, 1.0);
    thr_n = fast_and_slow(fast_slopes, slow_slopes, -1.0);
warning('on','signal:findpeaks:largeMinPeakHeight');

if isempty(thr_p) || isempty(thr_n)
    trans = [1 size(traces, 2)];
else
    % pair up positive/negative threshold, starting with positives
    thr = thr_p(1);
    j = 1;
    for i=2:length(thr_p)
        % if we are behind the next negative end, ignore
        if thr_p(i)<thr_n(j)
            continue
        end
        % if we still have negative ends after the current one
        if length(thr_n)>j
            % concatenate, current negative, curent positive
            thr = [thr, thr_n(j) thr_p(i)];
            
            % we are now looking for the next negative
            j = j + 1;
        end
    end
    % add the last negative
    thr = [thr, thr_n(j)];

    % shuffle around borders
    dx = 2*30;
    shuffled = [];
    for i=1:length(thr)
        shuffled = [shuffled, thr(i)-dx, thr(i)+dx];
    end
    trans = [1 shuffled size(traces,2)];
end

regions = unfold(trans, 2);

for i=1:size(regions,2)
    if i>1
        regions(1, i) = regions(1, i)+1; % so that regions do not overlap!
    end

    fprintf('Region %d, [%d,%d] is %7.2f s long.\n',...
        i, ...
        regions(1,i),regions(2,i),...
        (regions(2,i)-regions(1,i))/30);
end

data.regions = regions;
fprintf('Found regions in %5.3fs.\n',toc(trun));
end

function slopes = local_fits(what, trend_length, parforArg)
mm = unfold(what, trend_length);
xx = [ones(trend_length,1), (1:trend_length)'];
xx_op = (xx'* xx)\(xx');
slopes = zeros(1,length(what));
parfor (i=1:length(what)-trend_length, parforArg)
    yy = mm(:,i);
    beta = xx_op * yy;
    slopes(i) = beta(2);
end
slopes = filter(ones(1,trend_length)/trend_length,1,slopes);
slopes(1:60)=0;
end

function thr = fast_and_slow(fslope, sslope, s)
fmin = 3*std(fslope(s*fslope>0));
smin = 3*std(sslope(s*sslope>0));
[~, f_thr] = findpeaks(s*fslope, 'MINPEAKDISTANCE', 30*30, 'MINPEAKHEIGHT', 0.2);
[~, s_thr] = findpeaks(s*sslope, 'MINPEAKDISTANCE', 30*30, 'MINPEAKHEIGHT', 0.01);
thr = trim_small(sort(unique([f_thr, s_thr])));
end

function trimmed = trim_small(thr)
% remove small regions
dx = 30*30;
trimmed = thr;
trimmed([false (trimmed(2:end-1) - trimmed(1:end-2))<dx false])=[];
trimmed([(trimmed(2:end) - trimmed(1:end-1))<dx false]) = [];
end
