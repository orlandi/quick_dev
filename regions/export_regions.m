function export_regions( fout, data)
%EXPORT_REGIONS export stimulation region data

% header
fprintf(fout, '##\n##regions\n##\n');

for i=1:size(data.regions, 2)
    fprintf(fout, 'region:%d-%d\n', data.regions(1,i), data.regions(2,i));
end

end

