function savefig(n_data, n_fig, varargin)
global RUN

if ~isempty(varargin)
    figure(varargin{1});
end

f_path = [RUN.figure_path n_fig filesep];
if ~exist(f_path, 'file'), mkdir(f_path);end;

saveas( gcf, [f_path n_data '_' n_fig '.png']);
end


