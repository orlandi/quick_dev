function handles=select_files(handles, extension, label)

folder = get_default_path(handles);

[FileName,PathName,~] = uigetfile(extension, label, ...
    folder, 'MultiSelect', 'on');

if PathName
    handles = add_filenames_to_list(FileName, PathName, handles);
    handles.data_path = PathName;

    set_default_path(PathName);
end

end