function folder=get_default_path(handles)

folder = handles.data_path;
if (folder == 0)
    if exist('.lastread', 'file')
        fin=fopen('.lastread', 'r');
            folder=strtrim(fscanf(fin, '%s'));
        fclose(fin);
    else
        folder = 'c:\';
    end
end

end