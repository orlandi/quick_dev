function handles=add_filenames_to_list(filename, pathname, handles)

to_add = {};
if isa(filename, 'char')
    to_add = {filename};
elseif isa(filename, 'cell')
    to_add = filename;
end

for i=1:length(to_add)
    to_add{i} = [pathname to_add{i}];
end

if ~isempty(to_add)
    current = cellstr(get(handles.lst_queue, 'String'));
    set(handles.lst_queue, 'String',{current{:},to_add{:}});
end

end