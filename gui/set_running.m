function set_running(handles)
set(handles.running_waiting, 'String', 'Running');
set(handles.running_waiting, 'ForegroundColor', 'r');
drawnow;
end
