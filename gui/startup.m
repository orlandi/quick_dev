%% configure path

% Much easier
guiFolder = fileparts(mfilename('fullpath'));
addpath(genpath([guiFolder filesep '..'])); % Add everything below /gui to the path
rmpath(genpath([guiFolder filesep '..' filesep '.git'])) % But exclude .git/
%% global configuration for the script
global CONFIG

if ~isa(CONFIG, 'struct')
    CONFIG=struct();
end;

CONFIG.debug = 0;
CONFIG.quick = 0;

CONFIG.version = '0.7.0+';
CONFIG.magnification = 10; % Most movied are at 10X

CONFIG.preloadAllFrames = false; % If true, preload all the movie frames before using them (high memory cost) 
CONFIG.cellDetectionSpeedUp = true; % If true, only applies the median filter to the average frame (not to every single frame)
CONFIG.useParallelization = false; % If we use the parallel toolbox or not - doensn-t really work
CONFIG.minutes_to_sample = 1.5; % usually 1.5 (This should be heare and not hardcoded)
CONFIG.oopsi_peak_threshold = 0.5; % Added as a CONFIG parameter, detect as spikes anything above this threshold
CONFIG.useLUTcorrection = true; % Correct the LUT like ImageJ does on all shown average images (data is not altered)

CONFIG.frame_filter = fspecial('gaussian', [3 3]); % gaussian filter

CONFIG.back_strel = strel('ball', background_size(), background_size(), 0);

CONFIG.pool_size = 12;

CONFIG.max_frames = 128;            

CONFIG.cells_per_lab = 10;

CONFIG.min_firing_for_active = 3;

%CONFIG.per_frame = {@get_trace_gpu};
CONFIG.per_frame = {@get_trace};

CONFIG.burst_threshold = 0.1;

% global intensity threshold
CONFIG.glial_globalI_threshold = 0.25;

% local intensity threshold
CONFIG.glial_localI_threshold = 0.4;

% area threshold (pixel)
CONFIG.glial_maxArea_threshold = 80;
CONFIG.glial_minArea_threshold = 10;

% 1 = generate output video
% 0 = no output video
CONFIG.create_vid = 1;

% output video + fig consistency number
CONFIG.N = 1;

% number of frames to average = 1 new frame
CONFIG.num_avg_frames = 50;

% local field of view for counting
CONFIG.fov_x = 32;
CONFIG.fov_y = 32;


CONFIG.data_processing = {...
    @average_trace, ...
    @find_regions_slow, ...
    @find_peaks, ...
    @find_firing, ...
    @select_regions_raster, ...
    @regions_raster_trains, ...
    @regions_raster_intervals, ...
    @gliaAnalysis, ...
    %     @find_bursts, ...             %This one has a lot of problems...
    %    @find_bursts_regions_raster_selected, ...
    };

CONFIG.plots = {
    @plot_average_trace, ...
%     @plot_raster, ...
    %    @plot_activity_fraction, ...
    %    @plot_interburst, ...
    };

CONFIG.export = {...
    % @export_burst_stats, ...
    @export_cells, ...
    @export_regions, ...
    %    @export_train,...
    %    @export_regions_raster_trains, ...
    %    @export_regions_raster_intervals, ...
    %    @export_regions_raster_selected_burst_stats, ...
    };
