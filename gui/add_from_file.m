function handles=add_from_file(handles)
% ADD_FROM_FILE adds dataset names from a file.

[FileName,PathName,~] = uigetfile(...
    '*.txt', ...
    'Select source file', ...
    get_default_path(handles));

if PathName
    % reads the file and add all files to the list
    file_list = importdata([PathName filesep FileName], 'r');
    for i=1:length(file_list)
        [p, f, e] = fileparts(file_list{i});
        handles = add_filenames_to_list([f e], [p filesep], handles);
    end % for files

    set_default_path(PathName);
end % if path

end % function
