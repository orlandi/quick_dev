function handles=set_buttons_state(state)
btn_list = findobj('Style', 'pushbutton', '-or', 'Style', 'edit', ...
    '-or', 'Style', 'checkbox', '-or', 'Style', 'radiobutton');
for i=1:length(btn_list)
    set(btn_list, 'Enable', state);
end
    
end
