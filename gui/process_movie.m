function varargout = process_movie(varargin)
% PROCESS_MOVIE M-file for process_movie.fig
%      PROCESS_MOVIE, by itself, creates a new PROCESS_MOVIE or raises the existing
%      singleton*.
%
%      H = PROCESS_MOVIE returns the handle to a new PROCESS_MOVIE or the handle to
%      the existing singleton*.
%
%      PROCESS_MOVIE('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in PROCESS_MOVIE.M with the given input arguments.
%
%      PROCESS_MOVIE('Property','Value',...) creates a new PROCESS_MOVIE or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before process_movie_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to process_movie_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help process_movie

% Last Modified by GUIDE v2.5 11-Aug-2016 15:45:10

% Begin initialization code - DO NOT EDIT

gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @process_movie_OpeningFcn, ...
                   'gui_OutputFcn',  @process_movie_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT
end

% --- Executes just before process_movie is made visible.
function process_movie_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to process_movie (see VARARGIN)

startup;

global CONFIG

set(handles.lst_queue, 'String',{});

% Choose default command line output for process_movie
handles.output = hObject;

% user data
handles.data_path=0;

set_waiting(handles);
    
% open up the pool

% if we are using a version prior to r2014a
if verLessThan('matlab', '8.3')
    if (matlabpool('size') == 0)
        sched=findResource('scheduler','configuration', defaultParallelConfig);
        matlabpool('open',get(sched,'ClusterSize')-2);
    end
else % use the new way to create the pool
   if isempty(gcp('nocreate'))
       parpool('local');
   end       
end
% Update handles structure
guidata(hObject, handles);

% initialize parameter values 
set(handles.fov_x_input, 'string', CONFIG.fov_x);
set(handles.fov_y_input, 'string', CONFIG.fov_y);
set(handles.N_input, 'string', CONFIG.N);
set(handles.globalI_threshold_input, 'string', CONFIG.glial_globalI_threshold);
set(handles.localI_threshold_input, 'string', CONFIG.glial_localI_threshold);
set(handles.maxArea_input, 'string', CONFIG.glial_maxArea_threshold);
set(handles.minArea_input, 'string', CONFIG.glial_minArea_threshold);
set(handles.avgFrame_input, 'string', CONFIG.num_avg_frames);

if CONFIG.quadrants > 0
    set(handles.quadrants_checkbox, 'Value', 1.0);
end

if CONFIG.create_vid ==1
    set(handles.OF_vid_checkbox, 'Value', 1.0);
end
    
% initialize handles state
set(handles.quadrants_checkbox, 'enable', 'off');
set(handles.OF_vid_checkbox, 'enable', 'off');

end

% UIWAIT makes process_movie wait for user response (see UIRESUME)
% uiwait(handles.figure1);

% --- Outputs from this function are returned to the command line.
function varargout = process_movie_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;
end

% --- Executes on button press in btn_add_movie.
function btn_add_movie_Callback(hObject, eventdata, handles)
% hObject    handle to btn_add_movie (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles=select_files(handles, '*.avi;*.mpg','Add movies...');
guidata(hObject, handles);
end

% --- Executes on button press in btn_add_figure.
function btn_add_figure_Callback(hObject, eventdata, handles)
% hObject    handle to btn_add_figure (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

handles=select_files(handles, '*_raster.png','Add figures...');
guidata(hObject, handles);

end % function

% --- Executes on button press in btn_run_it.
function btn_run_it_Callback(hObject, eventdata, handles)
% hObject    handle to btn_run_it (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    global CONFIG
    function append_status(status_text)
        status = [get(handles.status, 'String') 13 status_text];
        set(handles.status, 'String', status);
        drawnow
    end
    set(handles.status, 'String', '' );

    data_files = cellstr(get(handles.lst_queue, 'String'));
    
    set_buttons_state('Off');
    
    adjustOutputList();
    
    set(handles.status, 'String', 'Processing movies.');
    set_running(handles);
    for k=1:length(data_files)
        try
            to_process = data_files{k};
            status = sprintf('Running movie %s (%d of %d)', to_process, k, length(data_files));
            set(handles.status, 'String', [get(handles.status, 'String') 13 status]);
            
            processing(to_process, ...
                CONFIG.per_frame, ...
                CONFIG.data_processing, ...
                CONFIG.plots, ...
                CONFIG.export);
        catch ME1
            %%% nothing to do really...
            st = get(handles.status, 'String');
            
            set(handles.status, 'String', ...
                [st 'ERROR!']);
            
            getReport(ME1, 'extended')
            %rethrow(ME1);
        end
        if exist('out', 'var')
            clear out
        end
    end

    set(handles.status, 'String', [get(handles.status, 'String') 13 'Done.']);
    set_waiting(handles);
    set_buttons_state('On');
end    

function status_Callback(hObject, eventdata, handles)
% hObject    handle to status (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of status as text
%        str2double(get(hObject,'String')) returns contents of status as a double
end


% --- Executes during object creation, after setting all properties.
function status_CreateFcn(hObject, eventdata, handles)
% hObject    handle to status (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
end

% --- Executes on button press in btn_remove_selected.
function btn_remove_selected_Callback(hObject, eventdata, handles)
% hObject    handle to btn_remove_selected (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
current = cellstr(get(handles.lst_queue, 'String'));
selected = get(handles.lst_queue, 'Value');
filter = not(ismember(1:length(current), selected));
set(handles.lst_queue, 'String', current(filter));
set(handles.lst_queue, 'Value', []);
guidata(hObject, handles);

end %function

% --- Executes on selection change in lst_queue.
function lst_queue_Callback(hObject, eventdata, handles)
% hObject    handle to lst_queue (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns lst_queue contents as cell array
%        contents{get(hObject,'Value')} returns selected item from lst_queue
end %function

% --- Executes during object creation, after setting all properties.
function lst_queue_CreateFcn(hObject, eventdata, handles)
% hObject    handle to lst_queue (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% starts empty
%%set(hObject, 'String',{});
end %function

% --- Executes on selection change in lst_data_process.
function lst_data_process_Callback(hObject, eventdata, handles)
% hObject    handle to lst_data_process (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns lst_data_process contents as cell array
%        contents{get(hObject,'Value')} returns selected item from lst_data_process

end %fnction

% --- Executes during object creation, after setting all properties.
function lst_data_process_CreateFcn(hObject, eventdata, handles)
% hObject    handle to lst_data_process (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

end %function

% --- Executes on selection change in lst_plots.
function lst_plots_Callback(hObject, eventdata, handles)
% hObject    handle to lst_plots (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns lst_plots contents as cell array
%        contents{get(hObject,'Value')} returns selected item from lst_plots

end % function

% --- Executes during object creation, after setting all properties.
function lst_plots_CreateFcn(hObject, eventdata, handles)
% hObject    handle to lst_plots (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

end %function


% --- Executes on button press in btn_from_file.
function btn_from_file_Callback(hObject, eventdata, handles)
% hObject    handle to btn_from_file (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles = add_from_file(handles);
guidata(hObject, handles);
end %from_file callback


% --- Executes on button press in chk_skip_region.
function chk_skip_region_Callback(hObject, eventdata, handles)
% hObject    handle to chk_skip_region (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of chk_skip_region
end


% --- Executes on button press in checkbox_plot_average_trace.
function checkbox_plot_average_trace_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox_plot_average_trace (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox_plot_average_trace

end
% --- Executes on button press in checkbox_export_regions.
function checkbox_export_regions_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox_export_regions (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox_export_regions
end

% --- Executes on button press in checkbox_export_cells.
function checkbox_export_cells_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox_export_cells (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox_export_cells
end

% --- Executes on button press in checkbox_opticalFlow.
function checkbox_opticalFlow_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox_opticalFlow (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox_opticalFlow

% ------------------------------------
% if selected, turn on "Glia analysis parameters"
if get(hObject, 'value') == 1
    set(handles.quadrants_checkbox, 'enable', 'on');
    set(handles.OF_vid_checkbox, 'enable', 'on');
    set(handles.N_input, 'enable', 'on');
    set(handles.globalI_threshold_input, 'enable', 'on');
    set(handles.localI_threshold_input, 'enable', 'on');
    set(handles.maxArea_input, 'enable', 'on');
    set(handles.minArea_input, 'enable', 'on');
    set(handles.avgFrame_input, 'enable', 'on');
    set(handles.fov_x_input, 'enable', 'on');
    set(handles.fov_y_input, 'enable', 'on');
else 
	set(handles.quadrants_checkbox, 'enable', 'off');
    set(handles.OF_vid_checkbox, 'enable', 'off');
    set(handles.N_input, 'enable', 'off');
    set(handles.globalI_threshold_input, 'enable', 'off');
    set(handles.localI_threshold_input, 'enable', 'off');
    set(handles.maxArea_input, 'enable', 'off');
    set(handles.minArea_input, 'enable', 'off');
    set(handles.avgFrame_input, 'enable', 'off');
    set(handles.fov_x_input, 'enable', 'off');
    set(handles.fov_y_input, 'enable', 'off');
end
end


function globalI_threshold_input_Callback(hObject, eventdata, handles)
% hObject    handle to globalI_threshold_input (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of globalI_threshold_input as text
%        str2double(get(hObject,'String')) returns contents of globalI_threshold_input as a double

global CONFIG
CONFIG.glial_globalI_threshold = str2double(get(hObject, 'String'));

end
% --- Executes during object creation, after setting all properties.
function globalI_threshold_input_CreateFcn(hObject, eventdata, handles)
% hObject    handle to globalI_threshold_input (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
set(hObject, 'enable', 'off');
end

function localI_threshold_input_Callback(hObject, eventdata, handles)
% hObject    handle to localI_threshold_input (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of localI_threshold_input as text
%        str2double(get(hObject,'String')) returns contents of localI_threshold_input as a double
global CONFIG
CONFIG.glial_localI_threshold = str2double(get(hObject, 'String'));

end

% --- Executes during object creation, after setting all properties.
function localI_threshold_input_CreateFcn(hObject, eventdata, handles)
% hObject    handle to localI_threshold_input (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
set(hObject, 'enable', 'off');
end



function maxArea_input_Callback(hObject, eventdata, handles)
% hObject    handle to maxArea_input (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of maxArea_input as text
%        str2double(get(hObject,'String')) returns contents of maxArea_input as a double
global CONFIG
CONFIG.glial_maxArea_threshold = str2num(get(hObject, 'String'));

end

% --- Executes during object creation, after setting all properties.
function maxArea_input_CreateFcn(hObject, eventdata, handles)
% hObject    handle to maxArea_input (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
set(hObject, 'enable', 'off');
end



function minArea_input_Callback(hObject, eventdata, handles)
% hObject    handle to minArea_input (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of minArea_input as text
%        str2double(get(hObject,'String')) returns contents of minArea_input as a double
global CONFIG
CONFIG.glial_minArea_threshold = str2num(get(hObject, 'String'));
end

% --- Executes during object creation, after setting all properties.
function minArea_input_CreateFcn(hObject, eventdata, handles)
% hObject    handle to minArea_input (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
set(hObject, 'enable', 'off');
end


function N_input_Callback(hObject, eventdata, handles)
% hObject    handle to N_input (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of N_input as text
%        str2double(get(hObject,'String')) returns contents of N_input as a double
global CONFIG
CONFIG.N = str2num(get(hObject, 'String'));

end

% --- Executes during object creation, after setting all properties.
function N_input_CreateFcn(hObject, eventdata, handles)
% hObject    handle to N_input (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
set(hObject, 'enable', 'off');
end



function avgFrame_input_Callback(hObject, eventdata, handles)
% hObject    handle to avgFrame_input (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of avgFrame_input as text
%        str2double(get(hObject,'String')) returns contents of avgFrame_input as a double
global CONFIG
CONFIG.num_avg_frames = str2num(get(hObject, 'String'));

end

% --- Executes during object creation, after setting all properties.
function avgFrame_input_CreateFcn(hObject, eventdata, handles)
% hObject    handle to avgFrame_input (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
set(hObject, 'enable', 'off');
end


% --- Executes on button press in quadrants_checkbox.
function quadrants_checkbox_Callback(hObject, eventdata, handles)
% hObject    handle to quadrants_checkbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of quadrants_checkbox
global CONFIG
quadrants = get(hObject, 'Value');

if quadrants == 1
    CONFIG.quadrants = 4;
else
    CONFIG.quadrants = 0;
end

end


% --- Executes on button press in OF_vid_checkbox.
function OF_vid_checkbox_Callback(hObject, eventdata, handles)
% hObject    handle to OF_vid_checkbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of OF_vid_checkbox
global CONFIG
CONFIG.create_vid = get(hObject, 'Value');

end



function fov_x_input_Callback(hObject, eventdata, handles)
% hObject    handle to fov_x_input (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of fov_x_input as text
%        str2double(get(hObject,'String')) returns contents of fov_x_input as a double
global CONFIG
CONFIG.fov_x = str2num(get(hObject, 'String'));

end


% --- Executes during object creation, after setting all properties.
function fov_x_input_CreateFcn(hObject, eventdata, handles)
% hObject    handle to fov_x_input (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
set(hObject, 'enable', 'off');
end



function fov_y_input_Callback(hObject, eventdata, handles)
% hObject    handle to fov_y_input (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of fov_y_input as text
%        str2double(get(hObject,'String')) returns contents of fov_y_input as a double
global CONFIG
CONFIG.fov_y = str2num(get(hObject, 'String'));
end


% --- Executes during object creation, after setting all properties.
function fov_y_input_CreateFcn(hObject, eventdata, handles)
% hObject    handle to fov_y_input (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
set(hObject, 'enable', 'off');
end
