close all
clear

startup;
%%
global CONFIG
run=1;
nbFiles = 0;
FileName = cell(nbFiles);
PathName = cell(nbFiles);
%------------- program structure
% 1. OPEN MOVIE TO BE ANALYZED
while run ~= 0
    extension={'*.avi';'*.dcimg'}; % Adding dcimg reader capabilities
    label='Select movie file';
    nbFiles = nbFiles+1;
    [FileName{nbFiles}, PathName{nbFiles}] = uigetfile(extension, label);
    
    if FileName{nbFiles}==0
        nbFiles = nbFiles-1;
        break;
    end
end

% 1.a) confirm selections

while run~=0
    if nbFiles==0
        break;
    end
    disp('Files to process:');
    for i=1:nbFiles
       str = [num2str(i) '. ' FileName{i}];
       disp(str);
    end
    
    answer = input('Ready to process these? (Y/N)', 's');
    if strcmpi(answer, 'n')
        answer = input('Which file do you want to remove?');
    elseif strcmpi(answer, 'y')
        break;
    else
       disp('Sorry, wrong input.');
       continue;
    end
    
    for i=answer:nbFiles
        FileName{i} = FileName{i+1};
    end
    FileName{nbFiles} = '';
    nbFiles = nbFiles-1;
end

% 2. Proceed to process selected files.
if nbFiles > 0                    
    for nb=1:nbFiles
        str = fullfile(PathName{nb}, FileName{nb});
        disp(['Currently processing: ', str]);
        
        try
            processing(str, ...
                CONFIG.per_frame, ...
                CONFIG.data_processing, ...
                CONFIG.plots, ...
                CONFIG.export);
        catch ME1
            getReport(ME1, 'extended')
            %rethrow(ME1);
        end
        
    end
    clear
else
    disp('No movie files were selected');   % no file
    return;
end
