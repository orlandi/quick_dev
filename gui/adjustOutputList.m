function adjustOutputList()
%GATHERSTARTUPPARAMETERS Summary of this function goes here
%   Detailed explanation goes here

global CONFIG
outputTypes = {'export', 'plot'};

outputHandles = findobj('Style', 'radiobutton');

for i=1:length(outputHandles)
    name = outputHandles(i).Tag;
    name = strrep(name, 'checkbox_', '@');
    value = outputHandles(i).Value;
    
    if value == 0           % remove job
        % check to see if this handle is for export, or plot work list.
        if ~isempty(strfind(name, outputTypes(1)))               % go into export and remove job if value = 0
            for l=1:length(CONFIG.export)                        % locate job in work list
                if ~isempty(strfind(name, char(CONFIG.export{l})))
                    CONFIG.export{l} = '';
                    break;
                end
            end
            CONFIG.export = CONFIG.export(~cellfun('isempty', CONFIG.export));      % remove empty cell
            
        elseif ~isempty(strfind(name, outputTypes(2)))              % go into plots and remove job if value = 0
            for l=1:length(CONFIG.plots)                            % locate job in work list
                if ~isempty(strfind(name, char(CONFIG.plots{l})))
                    CONFIG.plots{l} = '';
                    break;
                end
            end
            CONFIG.plots = CONFIG.plots(~cellfun('isempty', CONFIG.plots));      % remove empty cell
            
        else                                                        % not in either lists, therefore part of data_processing worklist
            for l=1:length(CONFIG.data_processing)                            % locate job in work list
                if ~isempty(strfind(name, char(CONFIG.data_processing{l})))
                    CONFIG.data_processing{l} = '';
                    break;
                end
            end
        end
    elseif value==1         % add job
        found=0;
        
        if ~isempty(strfind(name, outputTypes(1)))               % go into export and add job if it doesn't already exist
            for l=1:length(CONFIG.export)
                if ~isempty(strfind(name, char(CONFIG.export{l})))
                    found=1;
                    break;
                end
            end
            if found==0                                 %couldn't find it, therefore add it
                index = length(CONFIG.export)+1;
                CONFIG.export{index} = str2func(name);               % convert str to function handle
            end             % else found job, don't add again
            
        elseif ~isempty(strfind(name, outputTypes(2)))              % go into plots and remove job if value = 0
            for l=1:length(CONFIG.plots)
                if ~isempty(strfind(name, char(CONFIG.plots{l})))
                    found=1;
                    break;
                end
            end
            if found==0
                index = length(CONFIG.plots)+1;
                CONFIG.plots{index} = str2func(name);
            end
        else                                                        % not in either lists, therefore part of data_processing worklist
            for l=1:length(CONFIG.data_processing)
                if ~isempty(strfind(name, char(CONFIG.data_processing{l})))
                    found=1;
                    break;
                end
            end
            if found==0
                index = length(CONFIG.data_processing)+1;
                CONFIG.data_processing{index} = str2func(name);
            end
        end
    end
end

end

