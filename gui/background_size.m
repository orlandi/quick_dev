function [ back_size ] = background_size()
%background_size returns the size of the background, for the given config
global CONFIG

if CONFIG.magnification == 10
    back_size = 16; 
elseif CONFIG.magnification == 5
    back_size = 8;
else
    back_size = -1;
end

end

