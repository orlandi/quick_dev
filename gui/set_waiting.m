function set_waiting(handles)
set(handles.running_waiting, 'String', 'Waiting');
set(handles.running_waiting, 'ForegroundColor', 'g');
end
