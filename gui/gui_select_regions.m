function varargout = gui_select_regions(varargin)
% GUI_SELECT_REGIONS MATLAB code for gui_select_regions.fig
%      GUI_SELECT_REGIONS, by itself, creates a new GUI_SELECT_REGIONS or 
%      raises the existing
%      singleton*.
%
%      H = GUI_SELECT_REGIONS returns the handle to a new GUI_SELECT_REGIONS 
%      or the handle to
%      the existing singleton*.
%
%      GUI_SELECT_REGIONS('CALLBACK',hObject,eventData,handles,...) calls 
%      the local
%      function named CALLBACK in GUI_SELECT_REGIONS.M with the given input 
%      arguments.
%
%      GUI_SELECT_REGIONS('Property','Value',...) creates a new 
%      GUI_SELECT_REGIONS or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before gui_select_regions_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to gui_select_regions_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help gui_select_regions

% Last Modified by GUIDE v2.5 20-Sep-2012 13:38:07

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @gui_select_regions_OpeningFcn, ...
                   'gui_OutputFcn',  @gui_select_regions_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT
end

% --- Executes just before gui_select_regions is made visible.
function gui_select_regions_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to gui_select_regions (see VARARGIN)

% Choose default command line output for gui_select_regions
handles.output = hObject;

handles.lines = [];
handles.points = [];

% Update handles structure
guidata(hObject, handles);

% This sets up the initial plot - only do when we are invisible
% so window can get raised using gui_select_regions.
if strcmp(get(hObject,'Visible'),'off')
    cla(handles.ax_raster);
    axes(handles.ax_raster);

%   set(handles.ax_raster, 'NextPlot', 'add');
    p = get(handles.base_gui, 'UserData');
    if isempty(p)
        random_plt();
    else
        p{1}(p{2});
    end
    set(handles.ax_raster, 'ButtonDownFcn', @ButtonDownFcn);
    drawnow;
end

end

% --- Outputs from this function are returned to the command line.
function varargout = gui_select_regions_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% UIWAIT makes gui_select_regions wait for user response (see UIRESUME)
uiwait(handles.base_gui);

% Get default command line output from handles structure
h = guidata(hObject);
varargout{1} = h.output;
varargout{2} = h.points;

close
end

% --- Executes on button press in btn_get_regions.
function btn_get_regions_Callback(hObject, eventdata, handles)
% hObject    handle to btn_get_regions (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% outuput an even number of points
nr = length(handles.points);
nr = nr - mod(nr, 2);

handles.points = handles.points(1:nr);
guidata(hObject, handles);

uiresume;

set_buttons_state('off');

drawnow;
end

% --- Executes on mouse press over axes background.
function ButtonDownFcn(hObject, eventdata)
% hObject    handle to ax_raster (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

pos = get(hObject, 'CurrentPoint');
pos = pos(1,1);

kind=get(gcf, 'SelectionType');
if strfind(kind, 'normal')
    kind=1;
else
    kind=0;
end

axes(hObject);

handles = guidata(hObject);

% add line with left click, remove nearest with right
if kind
    handles.points = sort([handles.points pos]);
elseif ~isempty(handles.points)
    dst = abs(handles.points - pos);
    nearest = find(dst == min(dst));
    handles.points(nearest(1)) = [];
end

handles = update_lines(handles);
guidata(hObject, handles);

end

function handles=update_lines(handles)
% clean current lines
arrayfun(@delete, handles.lines);

% functions to draw the lines
    function l=draw_v_line(p)
        l=line([p p],ylim, 'Color','r');
    end
    function l=draw_h_line(p1,p2)
        y=mean(ylim);
        l=line([p1 p2],[y y], 'Color','b');
    end
% vertical lines for each selected point
vl = arrayfun(@draw_v_line, handles.points);

% horizontal lines between consecutive points
ps = handles.points(1:2:end); pf=handles.points(2:2:end);
nr = min(length(ps), length(pf));

hl = arrayfun(@draw_h_line, ps(1:nr), pf(1:nr));

handles.lines=[vl hl];
end

function random_plt()
    %TODO put a rasterplot
    plot(rand(5),'HitTest', 'off');
end
