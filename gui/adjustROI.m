function [ newFrames ] = adjustROI( frames, q )
%ADJUSTROI: inorder to save time, instead of manually cutting the videos
%from 1024x1024 pixels to 512x512 pixels, this function does the separating
%into quadrants of 512x512 for each movies loaded. 
%   q: the quadrant variable and depending on which quadrant we're in, we
%   cut the region of interest (roi) to be different on the full 1024x1024
%   videos. 
%   frames: the full 1024x1024 movie frames.
%   newFrames: returns frames for the specific quandrant. 

if q == 1
	roi = [512 1 511 511];
elseif q == 2
	roi = [1 1 511 511];
elseif q == 3
    roi = [1 512 511 511] ;
elseif q == 4
    roi = [512 512 511 511] ;
elseif q == 0
    newFrames = frames;
    return
end

for i=1:size(frames,3)
    newFrames(:,:,i) = imcrop(frames(:,:,i), roi);
end

end

